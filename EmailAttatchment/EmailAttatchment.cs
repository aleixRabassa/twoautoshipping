﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EmailAttachment
{
    class EmailAttachment
    {
        static void Main(string[] args)
        {
            string strTO;
            string strCC;
            string strBCC;
            string strSUBJECT;
            string strBODY;
            string strSMTPHOST;
            int intPORT;
            string strUSR;
            string strPWD;
            string strATTACHMENT;
            string strFROM;
            string strSSL;
            string strHTML;

            strTO = args[0];
            strCC = args[1];
            strBCC = args[2];
            strSUBJECT = args[3];
            strBODY = args[4];
            strSMTPHOST = args[5];
            intPORT = Convert.ToInt16(args[6]);
            strUSR = args[7];
            strPWD = args[8];
            strATTACHMENT = args[9];
            strFROM = args[10];
            strSSL = args[11];
            strHTML = args[12];

            //foreach (string s in args)
            //{
            //    System.Console.WriteLine(s);
            //}

            System.Console.WriteLine("Configuración del email:");
            System.Console.WriteLine("======================================================");
            System.Console.WriteLine("Servidor SMTP: " + strSMTPHOST);
            System.Console.WriteLine("Puerto salida SMTP: " + intPORT);
            System.Console.WriteLine("Usuario: " + strUSR);
            System.Console.WriteLine("Contraseña: " + strPWD);
            System.Console.WriteLine("SSL: " + strSSL);
            System.Console.WriteLine("HTML: " + strHTML);
            System.Console.WriteLine("======================================================");
            System.Console.WriteLine("De: " + strFROM);
            System.Console.WriteLine("Para: " + strTO);
            System.Console.WriteLine("CC: " + strCC);
            System.Console.WriteLine("BCC: " + strBCC);
            System.Console.WriteLine("Asunto: " + strSUBJECT);
            System.Console.WriteLine("Adjunto: " + strATTACHMENT);  
            System.Console.WriteLine("Cuerpo: " + strBODY);

            Email.SCM_SendMail_Attachment(strTO, strCC, strBCC, strSUBJECT, strBODY, strSMTPHOST, intPORT, strUSR, strPWD, strATTACHMENT, strFROM, strSSL, strHTML);

        }
    }
}
