﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EmailAttachment
{
    class Email
    {
        public static void SCM_SendMail_Attachment(string _To, string _CC, string _BCC, string _Subject, string _Body, string _SmtpHost, int _SmtpPort, string _SmtpUsr, string _SmtpPwd, string _Attachment, string _From, string _SSL, string _HTML)
        {
            //Declaramos las variables
            string strTO;
            string strCC;
            string strBCC;
            string strSBJECT;
            string strBODY;
            string strSMTPHost;
            string strSMTPUsr;
            string strSMTPPwd;
            string strMail;
            string strPort;
            string strAttachment;
            string strSSL;
            string strHTML;

            //Recuperamos los datos.
            strTO = _To.ToString();
            strCC = _CC.ToString();
            strBCC = _BCC.ToString();
            strSBJECT = _Subject.ToString();
            strBODY = _Body.ToString();
            strSMTPHost = _SmtpHost.ToString();
            strSMTPUsr = _SmtpUsr.ToString();
            strSMTPPwd = _SmtpPwd.ToString();
            strPort = _SmtpPort.ToString();
            strAttachment = _Attachment.ToString();
            strMail = _From.ToString();
            strSSL = _SSL.ToString();
            strHTML = _HTML.ToString();


            // Definimos un email nuevo.
            Mail emlSCMMail = new Mail();

            // Si lleva un path de archivo.
            if (!string.IsNullOrEmpty(strAttachment))
            {
                emlSCMMail.AddAttachments(strAttachment);
            }

                if (!string.IsNullOrEmpty(strTO))
                {
                    if (strTO.Contains(";"))
                    {
                        //Separar direcciones
                        string[] Direcciones = strTO.Split(';');

                        for (int k = 0; k <= Direcciones.Length - 1; k++)
                        {
                            Direcciones[k] = Direcciones[k].Replace(" ", "");
                            emlSCMMail.AddAddressTo(new System.Net.Mail.MailAddress(Direcciones[k], Direcciones[k]));
                        }
                    }
                    else
                    {
                        emlSCMMail.AddAddressTo(new System.Net.Mail.MailAddress(strTO, strTO));
                    }
                }

                if (!string.IsNullOrEmpty(strCC))
                {
                    if (strCC.Contains(";"))
                    {
                        //Separar direcciones
                        string[] Direcciones = strCC.Split(';');

                        for (int k = 0; k <= Direcciones.Length - 1; k++)
                        {
                            Direcciones[k] = Direcciones[k].Replace(" ", "");
                            emlSCMMail.AddAddressCC(new System.Net.Mail.MailAddress(Direcciones[k], Direcciones[k]));
                        }
                    }
                    else
                    {
                        emlSCMMail.AddAddressCC(new System.Net.Mail.MailAddress(strCC, strCC));
                    }
                }
                if (!string.IsNullOrEmpty(strBCC))
                {
                    if (strBCC.Contains(";"))
                    {
                        //Separar direcciones
                        string[] Direcciones = strBCC.Split(';');

                        for (int k = 0; k <= Direcciones.Length - 1; k++)
                        {
                            Direcciones[k] = Direcciones[k].Replace(" ", "");
                            emlSCMMail.AddAddressBCC(new System.Net.Mail.MailAddress(Direcciones[k], Direcciones[k]));
                        }
                    }
                    else
                    {
                        emlSCMMail.AddAddressBCC(new System.Net.Mail.MailAddress(strBCC, strBCC));
                    }
                }
                emlSCMMail.Body = strBODY;
                emlSCMMail.Subject = strSBJECT;

                //Configuramos con los datos cargados
                System.Net.NetworkCredential nCredentials = new System.Net.NetworkCredential();
                System.Net.Mail.MailAddress mAddress = new System.Net.Mail.MailAddress(strMail, strMail);

                nCredentials.UserName = strSMTPUsr;
                nCredentials.Password = strSMTPPwd;

                emlSCMMail.From = mAddress;
                emlSCMMail.Host = strSMTPHost;
                emlSCMMail.Port = Convert.ToInt32(strPort);
                emlSCMMail.ReplyTo = mAddress;
                emlSCMMail.Credentials = nCredentials;

                if (strSSL == "T")
                {
                    emlSCMMail.SSL = true;
                }
                if (strSSL == "F")
                {
                    emlSCMMail.SSL = false;
                }

                if (strHTML == "T")
                {
                    emlSCMMail.HTML = true;
                }
                if (strHTML == "F")
                {
                    emlSCMMail.HTML = false;
                }


                //Enviamos el email.
                emlSCMMail.send();                
            //}
        }
    }
}
