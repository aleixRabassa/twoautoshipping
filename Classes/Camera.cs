﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Spire.Barcode;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TwoAutoShipping
{
    class Camera
    {
        Globales.sqlTWO sqlShip;
        public VideoCapture capture;
        //public Mat lastFrame = new Mat();
        public Mat actualFrame = new Mat();
        public Mat firstFrame = new Mat();
        public int cameraId;
        public String cameraName;
        public String cameraPath;
        public int doorId;
        public int doorIndex;
        public String doorSide;        
        public Boolean play = false;        
        //public int threshold = 200000;
        //private int move = 0;        
        public PictureBox outterPictureBox;
        public CameraList outterList;
        Thread t;
        int mod;

        public Camera()
        {

        }

        public void Load()
        {
            try
            {
                int camIndex = 0;
                if ((File.Exists(cameraPath) || isIpAddress(cameraPath)) && !int.TryParse(cameraPath, out camIndex))
                {
                    // fichero o ip
                    capture = new VideoCapture(cameraPath);   
                }
                else if (int.TryParse(cameraPath, out camIndex))
                {
                    // webcam
                    capture = new VideoCapture(camIndex);
                }

                if (capture.IsOpened)
                {
                    capture.Read(actualFrame);
                    firstFrame = actualFrame;
                    t = new Thread(new ThreadStart(Playing));
                }
            }
            catch (Exception ex)
            {
                Globales.Mensajes.Msg(ex.Message);
            }
        }

        public void PlayPause(bool force = false)
        {
            // si force es true -> forzamos el estado de la lista
            if (force)
            {
                this.play = outterList.play;
            }
            else
            {
                this.play = !play;
            }

            try
            {
                if (capture.IsOpened)
                {                    
                    t.Abort();

                    // Si play es true iniciamos un nuevo thread
                    if (play)
                    {
                        t = new Thread(new ThreadStart(Playing));
                        t.Start();
                    }
                }
            }
            catch (Exception ex)
            {
                Globales.Mensajes.Msg(ex.Message);
            }
        }

        internal async void Playing()
        {
            double fps = capture.GetCaptureProperty(CapProp.Fps);
            double totalFrames = capture.GetCaptureProperty(CapProp.FrameCount);
            int frameNum = 0;

            while (play)
            {
                // si es video leemos el siguiente frame
                if (totalFrames != 1 && capture.Grab())
                {
                    capture.Retrieve(actualFrame);
                }

                frameNum += 1;

                if (!actualFrame.IsEmpty)
                {
                    //<deprecated>
                    //// si no es el primer frame comparamos.                        
                    //if (!lastFrame.IsEmpty)
                    //{
                    //    move = actualFrame.ToImage<Gray, byte>().AbsDiff(lastFrame.ToImage<Gray, byte>()).CountNonzero()[0];    // NOT WORKING                        
                    //    //actualThresholdLbl.Text = move.ToString();
                    //    //outterPictureBox.Image = actualFrame.ToImage<Gray, byte>().AbsDiff(lastFrame.ToImage<Gray, byte>()).Bitmap;
                    //}

                    // mostramos el siguiente frame y esperamos s/fps segundos.
                    if (outterPictureBox.InvokeRequired)
                    {
                        outterList.outterForm.FeedbackTbx.Invoke(
                           (Action)(() => outterPictureBox.Image = actualFrame.Bitmap)
                        );
                    }

                    if (fps <= 3)
                    {
                        mod = 1;
                    }
                    else
                    {
                        mod = Convert.ToInt16(fps / 3);
                    }

                    // escaneamos 3 frames por segundo independientemente del frame rate del video.
                    if (frameNum % mod == 0)
                    {
                        ScanBarcode();
                    }
                    else
                    {
                        // esperamos para el siguiente frame solo cuando no se escanee el actual
                        await Task.Delay(1000 / Convert.ToInt16(fps));
                    }

                    //<deprecated>
                    // si se detecta movimiento escaneamos.
                    //if (move > threshold)
                    //{
                    //    ScanBarcode();  
                    //}

                    //lastFrame = actualFrame;
                }
                else
                {
                    // cuando acaba el video volvemos a empezar                        
                    capture.SetCaptureProperty(CapProp.PosAviRatio, 0);
                    frameNum = 0;
                }
            }
        }

        private void ScanBarcode()
        {
            //string[] results = null;// = new String[0];
            //string resOne = null;

            if (actualFrame != null)
            {
                Stopwatch sw = Stopwatch.StartNew();
                sw.Start();

                // Returns as a string array all barcode found in the provided image.
                string[] results = BarcodeScanner.Scan(actualFrame.Bitmap, BarCodeType.DataMatrix);                        

                sw.Stop();

                //if (outterList.outterForm.FeedbackTbx.InvokeRequired)
                //{
                //    outterList.outterForm.FeedbackTbx.Invoke(
                //       (Action)(() => AddToFeedbackTbx(cameraName + ">> Scan elapsed time: " + sw.Elapsed))
                //    );
                //}

                if (results.Length > 0)
                {
                    foreach (string result in results)
                    {
                        if (outterList.outterForm.FeedbackTbx.InvokeRequired)
                        {
                            outterList.outterForm.FeedbackTbx.Invoke(
                                (Action)(() => AddToFeedbackTbx(cameraName + " >> Barcode detected: " + result))
                            );
                        }

                        // expedimos la UA       
                        string HUId = result;
                        string RouteSheetId = null;
                        bool HUHasBeenSent = false;
                        bool RSHasBeenSent = false;

                        if (Ship(ref HUId, ref RouteSheetId, ref HUHasBeenSent, ref RSHasBeenSent))
                        {
                            if (HUHasBeenSent && outterList.outterForm.FeedbackTbx.InvokeRequired)
                            {
                                outterList.outterForm.FeedbackTbx.Invoke(
                                    (Action)(() => AddToFeedbackTbx(cameraName + " >> UA expedida: " + HUId))
                                );
                            }

                            if (RSHasBeenSent && outterList.outterForm.FeedbackTbx.InvokeRequired)
                            {
                                outterList.outterForm.FeedbackTbx.Invoke(
                                    (Action)(() => AddToFeedbackTbx(cameraName + " >> Hoja " + RouteSheetId + " expedida."))
                                );
                            }
                        }
                    }
                }                
            }
        }

        private bool isIpAddress(string ipString)
        {
            IPAddress ip;

            try
            {
                ipString = ipString.Replace("http://", ""); //remove http://
                ipString = ipString.Replace("https://", ""); //remove https://
                ipString = ipString.Substring(0, ipString.IndexOf("/")); //remove everything after the first /
                ipString = ipString.Substring(0, ipString.IndexOf(":")); //remove everything after the first :
            }
            catch
            {

            }            

            return IPAddress.TryParse(ipString, out ip);
        }

        private bool Ship(ref string HUId, ref string RouteSheetId, ref bool HUHasBeenSent, ref bool RSHasBeenSent)
        {
            sqlShip = new Globales.sqlTWO(Globales.Definiciones.sqlConnectionG, "[dbo].[autoShippingShipHU]");
            sqlShip.Parameters["@HUID"].Value = HUId;
            sqlShip.Execute();

            HUId = sqlShip.Parameters["@HUId"].Value != null ? sqlShip.Parameters["@HUId"].Value.ToString() ?? null : null;
            RouteSheetId = sqlShip.Parameters["@RouteSheetId"].Value != null ? sqlShip.Parameters["@RouteSheetId"].Value.ToString() ?? null : null;

            if (sqlShip.Parameters["@HUHasBeenSent"].Value.ToString() == "1")
            {
                HUHasBeenSent = true;
            }

            if (sqlShip.Parameters["@RSHasBeenSent"].Value.ToString() == "1")
            {
                RSHasBeenSent = true;
            }

            return true;
        }

        private void AddToFeedbackTbx(string str)
        {
            outterList.outterForm.FeedbackTbx.Text += Environment.NewLine + str;
            outterList.outterForm.FeedbackTbx.SelectionStart = outterList.outterForm.FeedbackTbx.Text.Length;
            outterList.outterForm.FeedbackTbx.ScrollToCaret();
        }

        public int getDoorSideId()
        {
            switch (doorSide)
            {
                case "Left":    return 1;
                case "Top":     return 2;
                case "Right":   return 3;                 
                default:        return 1;
            }
        }
    }
}
