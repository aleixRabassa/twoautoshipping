﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.ExceptionServices;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace TwoAutoShipping
{   
    class CameraList
    {
        Globales.sqlTWO sqlCameraSettings;
        DataTable tblCameraSettings = new DataTable();
        public List<Camera> cameras = new List<Camera>();
        public Dictionary<int,int> doorIndices = new Dictionary<int,int>();
        public int totalDoors;
        public int maxCamerasPerDoor;
        public int cameraCount = 0;
        public Boolean play = false;
        //public int threshold = 200000;
        public AutoShippingForm outterForm;

        public CameraList()
        {
            
        }
        
        public void PlayPauseAll()
        {
            play = !play;

            foreach (Camera cam in cameras)
            {
                cam.PlayPause(play);
            }
        }
        
        public void LoadAll()
        {
            // if we reload during play status, we pause before reloading
            if (play)
            {
                outterForm.PlayPauseButton_Click(this, new EventArgs());
            }

            sqlCameraSettings = new Globales.sqlTWO(Globales.Definiciones.sqlConnectionG, "[dbo].[autoShippingCameraSettings_S]");
            tblCameraSettings = sqlCameraSettings.Fill();

            cameras.Clear();
            doorIndices.Clear();
            Camera newCam;
            foreach (DataRow camera in tblCameraSettings.Rows)
            {
                newCam = new Camera();
                newCam.cameraId   = Convert.ToInt16(camera["CameraId"]);
                newCam.cameraName = Convert.ToString(camera["CameraName"]);
                newCam.cameraPath = Convert.ToString(camera["CameraPath"]);
                newCam.doorId = Convert.ToInt16(camera["DoorId"]);
                newCam.doorSide   = Convert.ToString(camera["DoorSide"]);
                newCam.outterList = this;
                newCam.Load();
                cameras.Add(newCam);

                totalDoors = Convert.ToInt16(camera["TotalDoors"]);
                maxCamerasPerDoor = Convert.ToInt16(camera["MaxCamerasPerDoor"]);
                cameraCount++;     
                
                // damos valor a doorIndex para construir la matriz de cámaras.
                if (!doorIndices.ContainsKey(newCam.doorId))
                {
                    doorIndices.Add(newCam.doorId, doorIndices.Count + 1);                    
                }
                newCam.doorIndex = doorIndices[newCam.doorId];

                //lock(outterForm.FeedbackTbx.Text)
                //{
                    outterForm.FeedbackTbx.Text = newCam.cameraName + " loaded";
                //}
               
            }
        }

        //<deprecated>
        //internal void setThresholdAll(int th)
        //{
        //    threshold = th;

        //    foreach (Camera cam in cameras)
        //    {
        //        cam.threshold = threshold;
        //    }
        //}

        public string toString()
        {
            string str = "";

            foreach (Camera cam in cameras)
            {
                str += "{" + cam.cameraId + "," + cam.cameraName + "," + cam.cameraPath + "," + cam.doorId + "," + cam.doorSide + "}" + Environment.NewLine;
            }

            return str;
        }

    }
}
