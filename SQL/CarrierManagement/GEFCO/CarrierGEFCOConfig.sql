﻿CREATE TABLE [dbo].[CarrierGEFCOConfig] (
    [OwnerId]             NVARCHAR (50)  NULL,
    [WarehouseId]         NVARCHAR (50)  NULL,
    [CarrierDelegationId] NVARCHAR (50)  NULL,
    [FileNameStart]       NVARCHAR (255) NULL,
    [FTPServer]           NVARCHAR (50)  NULL,
    [FTPUser]             NVARCHAR (25)  NULL,
    [FTPPassword]         NVARCHAR (25)  NULL,
    [FTPPathBinaries]     NVARCHAR (255) NULL,
    [FTPGenPath]          NVARCHAR (255) NULL,
    [FTPOutPath]          NVARCHAR (255) NULL,
    [MailFrom]            NVARCHAR (255) NULL,
    [MailTo]              NVARCHAR (255) NULL,
    [MailCC]              NVARCHAR (255) NULL,
    [MailBCC]             NVARCHAR (255) NULL,
    [MailSubject]         NVARCHAR (255) NULL,
    [isFTPSender]         BIT            DEFAULT ((0)) NULL,
    [isMailSender]        BIT            DEFAULT ((0)) NULL,
    [CreationDate]        DATETIME       DEFAULT (getdate()) NOT NULL,
    [rowid]               BIGINT         IDENTITY (1, 1) NOT NULL
);


