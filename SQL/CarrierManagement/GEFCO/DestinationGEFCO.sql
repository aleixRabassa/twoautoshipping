﻿CREATE TYPE [dbo].[DestinationGEFCO] AS TABLE
(
	[CountryId]			NVARCHAR (25),
	[PostalCode]		NVARCHAR (25),
	[City]				NVARCHAR (50),
	[Carrier]		    NVARCHAR (25),
	[TrafficCode]		NVARCHAR (25),
	[DirectionalCode]   NVARCHAR (25),
	[BayCode]			NVARCHAR (25),
	[CDIR]				NVARCHAR (25),
	[ActionCode]		NVARCHAR (25),
	[Free1]				NVARCHAR (25),
	[Free2]				NVARCHAR (25),
	[Free3]				NVARCHAR (25)
)