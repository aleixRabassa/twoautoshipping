﻿CREATE TABLE [dbo].[CarrierGEFCODestinations]
(
	[CountryId]			NVARCHAR (25)    NULL,
	[PostalCode]		NVARCHAR (25)    NULL,
	[City]				NVARCHAR (50)    NULL,
	[Carrier]			NVARCHAR (25)    NULL,
	[TrafficCode]		NVARCHAR (25)    NULL,
	[DirectionalCode]   NVARCHAR (25)    NULL,
	[BayCode]			NVARCHAR (25)    NULL,
	[CDIR]				NVARCHAR (25)    NULL,
	[ActionCode]		NVARCHAR (25)    NULL,
	[Free1]				NVARCHAR (25)    NULL,
	[Free2]				NVARCHAR (25)    NULL,
	[Free3]				NVARCHAR (25)    NULL
)
