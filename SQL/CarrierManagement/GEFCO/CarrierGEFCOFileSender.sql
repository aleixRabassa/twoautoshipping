﻿CREATE PROCEDURE [dbo].[CarrierGEFCOFileSender]
/******************************************
Los parámetros isMailSender y isFTPSender en la tabla CarrierCBLConfig determinan cómo se va a enviar. 
*******************************************/
	--ENTRA
	@CODHJA NVARCHAR (25) = NULL
	--SALIDA

AS
	--VARIABLES DE CONTROL
	DECLARE @_FullFilePath AS NVARCHAR(512);
	DECLARE @_FullFilePath2 AS NVARCHAR(512);
	DECLARE @_PathBinaries AS NVARCHAR(512);
	DECLARE @_FileExists AS INT;
	DECLARE @_DirectoryProperty AS TABLE (IsFile bit, IsDir bit, HasParent bit)
	DECLARE @ERR	AS INT				-- Retorno de resolución de funciones.
	--DECLARE @FECENV AS NVARCHAR(6)		-- Fecha Entrega
	DECLARE @_EXEC AS NVARCHAR(255);
	DECLARE @_Path AS NVARCHAR(255);
	DECLARE @cmd AS NVARCHAR(1000);
	DECLARE @CODTRA AS NVARCHAR(25);
	DECLARE @CODEMP AS NVARCHAR(25)
	DECLARE @CODALM AS NVARCHAR(25)
	DECLARE @_FileName AS NVARCHAR(50);
	DECLARE @FTPServer AS NVARCHAR(50)
	DECLARE @FTPUser	AS NVARCHAR(50)	
	DECLARE @FTPPassword	AS NVARCHAR(50)	
	DECLARE @FTPOutPath	AS NVARCHAR(255)	
	DECLARE @isFTPSender AS BIT
	DECLARE @isMailSender AS BIT
	DECLARE @_SeurMailFrom AS NVARCHAR(255)
	DECLARE @_SmtpServer AS NVARCHAR(255)
	DECLARE @_SmtpPort AS NVARCHAR(10)
	DECLARE @_LogonUser AS NVARCHAR(255)
	DECLARE @_LogonPassword AS NVARCHAR(255)
	DECLARE @_SmtpEncryption AS CHAR(1)
	DECLARE @_DachserSeurMailTo AS NVARCHAR(255)										
	DECLARE @_DachserMailCC AS NVARCHAR(255)	
	DECLARE @_MailSubject AS NVARCHAR(255)	
	DECLARE @MailFrom AS NVARCHAR(255)	
	DECLARE @MailTo AS NVARCHAR(255)	
	DECLARE @MailCC AS NVARCHAR(255)	
	DECLARE @MailBCC AS NVARCHAR(255)	
	DECLARE @ProcessId AS UNIQUEIDENTIFIER
	SET @ProcessId = NEWID()
	--INICIO
	SET @ERR = -1
	IF EXISTS (SELECT TOP 1 1 FROM [dbo].[CarrierShippingCo] INNER JOIN [dbo].T230ENTSAL ON C230CODTRA = [CarrierDelegationId] WHERE C230CODHJA = @CODHJA AND [CarrierCode] = N'GEFCO') BEGIN --//Transportista CBL
		SELECT TOP 1 @CODEMP = C240CODEMP, @CODALM = C230CODALM, @CODTRA = C230CODTRA FROM T230ENTSAL INNER JOIN T240ENTSALLIN ON C230CODALM = C240CODALM AND C230CODHJA = @CODHJA
		SELECT @_PathBinaries = FTPPathBinaries FROM [dbo].[CarrierGEFCOConfig] WITH (READUNCOMMITTED) WHERE OwnerId = @CODEMP AND WarehouseId = @CODALM AND CarrierDelegationId = @CODTRA
		SET @_EXEC=	N'[dbo].[CarrierGEFCOFileFiller] @CODHJA=' + N'''' + @CODHJA + N''''

		PRINT '==============================================='
		PRINT 'CarrierGEFCOFileSender >> Generando fichero GEFCO de la hoja ' + @CODHJA
		PRINT 'CarrierGEFCOFileSender >>' + @_EXEC

		-- Generamos el nombre del fichero
		SELECT @_Path = FTPGenPath FROM [dbo].[CarrierGEFCOConfig] WITH (READUNCOMMITTED) WHERE OwnerId = @CODEMP AND WarehouseId = @CODALM AND CarrierDelegationId = @CODTRA
		SELECT @_FileName = ISNULL(FileNameStart,@CODHJA) + '_' + FORMAT(getdate(), 'yyyyMMdd') + '_' + FORMAT(getdate(), 'hhmmss') + N'.txt' FROM [dbo].[CarrierGEFCOConfig] WITH (READUNCOMMITTED) WHERE OwnerId = @CODEMP AND WarehouseId = @CODALM AND CarrierDelegationId = @CODTRA
		SELECT @_FullFilePath = @_Path + @_FileName

		PRINT 'CarrierGEFCOFileSender >> Creating file: ' + @_FullFilePath
		PRINT 'EXECUTE SCM_BulkFile @_FilePath = ' + @_FullFilePath + ', @_SqlCommand = ' + @_EXEC

		--// Copia de seguridad interna.
		EXECUTE SCM_BulkFile
		 @_FilePath = @_FullFilePath
		,@_SqlCommand = @_EXEC
		INSERT INTO @_DirectoryProperty EXEC master.[dbo].xp_fileexist @_FullFilePath
		SELECT @_FileExists = IsFile FROM @_DirectoryProperty	
						
		SELECT TOP 1 @isFTPSender = isFTPSender FROM [dbo].[CarrierGEFCOConfig] WITH (READUNCOMMITTED)
		SELECT TOP 1 @isMailSender = isMailSender FROM [dbo].[CarrierGEFCOConfig] WITH (READUNCOMMITTED)
		--Si hemos generado el fichero y es FTPSender lo enviamos por FTP
		IF (@_FileExists = 1 AND @isFTPSender = 1) BEGIN
			
			PRINT 'CarrierGEFCOFileSender >> Sending file via FTP'

			SELECT @FTPServer	= FTPServer		FROM [dbo].[CarrierGEFCOConfig] WITH (READUNCOMMITTED) WHERE OwnerId = @CODEMP AND WarehouseId = @CODALM AND CarrierDelegationId = @CODTRA
			SELECT @FTPUser		= FTPUser		FROM [dbo].[CarrierGEFCOConfig] WITH (READUNCOMMITTED) WHERE OwnerId = @CODEMP AND WarehouseId = @CODALM AND CarrierDelegationId = @CODTRA
			SELECT @FTPPassword	= FTPPassword	FROM [dbo].[CarrierGEFCOConfig] WITH (READUNCOMMITTED) WHERE OwnerId = @CODEMP AND WarehouseId = @CODALM AND CarrierDelegationId = @CODTRA
			SELECT @FTPOutPath	= ISNULL(FTPOutPath,N'')	FROM [dbo].[CarrierGEFCOConfig] WITH (READUNCOMMITTED) WHERE OwnerId = @CODEMP AND WarehouseId = @CODALM AND CarrierDelegationId = @CODTRA

			--Enviamos el fichero generado por ftp
			EXEC [dbo].[SCM_ftp_Putfile]		
				@FTPServer=@FTPServer,	
				@FTPUser=@FTPUser,				
				@FTPPWD=@FTPPassword,		
				@FTPPath=@FTPOutPath,	
				@FTPFileName=@_FileName,
				@workdir=@_Path,
				@SourcePath=N'',
				@SourceFile=@_FullFilePath

			INSERT INTO SentFiles (RouteSheetId, CarrierCode) VALUES (@CODHJA,@CODTRA)
			UPDATE SentFiles SET SentBy = N'FTP', ProcessId = @ProcessId WHERE ProcessId IS NULL
		END	
		ELSE IF (@_FileExists = 1 AND @isMailSender = 1) BEGIN
			--// Recuperamos datos para mail		
			PRINT 'CarrierGEFCOFileSender >> Sending file via email'			
			SET @ProcessId = NEWID()

			SELECT TOP 1
				@MailTo		= [MailTo],
				@MailFrom	= [MailFrom],
				@MailCC		= [MailCC],
				@MailBCC	= [MailBCC],
				@_MailSubject = [MailSubject]
			FROM
				[dbo].[CarrierGEFCOConfig] WITH (READUNCOMMITTED)

			SELECT TOP 1				
				@_SmtpServer  = [SmtpServer],
				@_SmtpPort    = CAST([SmtpPort] AS NVARCHAR),
				@_LogonUser   = [LogonUser],
				@_LogonPassword	= [LogonPassword],
				@_SmtpEncryption = CASE WHEN [SmtpEncryption] = 1 THEN 'T' ELSE 'F' END
			FROM
				[dbo].[setMailOutgoing]
			WHERE FromAddress = @MailFrom
				
			--Enviamos el fichero generado por mail
			--SELECT @cmd = @_PathBinaries + N'EmailAttachment.exe "' + @MailTo + '" "' + @MailCC + '" "' + @MailBCC + '" "' + @_MailSubject + '" "" "' + @_SmtpServer + '" "' + @_SmtpPort + '" "' + @_LogonUser + '" "' + @_LogonPassword + '" "' + @_FullFilePath + '" "' + @MailFrom + '" "' + @_SmtpEncryption + '" "F"'
			SELECT @cmd = N'C:\SCM\EmailAttachment.exe "' + @MailTo + '" "' + @MailCC + '" "' + @MailBCC + '" "' + @_MailSubject + '" "" "' + @_SmtpServer + '" "' + @_SmtpPort + '" "' + @_LogonUser + '" "' + @_LogonPassword + '" "' + @_FullFilePath + '" "' + @MailFrom + '" "' + @_SmtpEncryption + '" "F"'
			PRINT N'@_PathBinaries: ' + ISNULL(@_PathBinaries, N'NULL')
			PRINT N'@MailTo: ' + ISNULL(@MailTo, N'NULL')
			PRINT N'@MailCC: ' + ISNULL(@MailCC, N'NULL')
			PRINT N'@_MailSubject: ' + ISNULL(@_MailSubject, N'NULL')
			PRINT N'@_SmtpServer: ' + ISNULL(@_SmtpServer, N'NULL')
			PRINT N'@_SmtpPort: ' + ISNULL(@_SmtpPort, N'NULL')
			PRINT N'@_LogonUser: ' + ISNULL(@_LogonUser, N'NULL')
			PRINT N'@_LogonPassword: ' + ISNULL(@_LogonPassword, N'NULL')
			PRINT N'@_FullFilePath: ' + ISNULL(@_FullFilePath, N'NULL')
			PRINT N'@MailFrom: ' + ISNULL(@MailFrom, N'NULL')
			PRINT N'@_SmtpEncryption: ' + ISNULL(@_SmtpEncryption, N'NULL')
			PRINT @cmd

			INSERT INTO SentFiles(Output)
			EXEC xp_cmdshell @cmd
			UPDATE SentFiles SET RouteSheetId = @CODHJA, CarrierCode = @CODTRA, SentBy = N'Email', CMDCommand = @cmd, ProcessId = @ProcessId WHERE ProcessId IS NULL
					
			----SELECT @cmd = 'move "' + @_FullFilePath + '" "' + REPLACE(@_FullFilePath,'.txt','_' + REPLACE(REPLACE(convert(varchar(20),GETDATE(),2)+convert(varchar(20),GETDATE(),108),'.',''),':','')+'.txt"')
			----EXEC xp_cmdshell @cmd
			----SELECT @cmd = 'move "' + REPLACE(@_FullFilePath,'.txt','_' + REPLACE(REPLACE(convert(varchar(20),GETDATE(),2)+convert(varchar(20),GETDATE(),108),'.',''),':','')+ '.txt" "' + @_PathHistorical + '"')
			----EXEC xp_cmdshell @cmd
			--SELECT @cmd = 'move "' + @_FullFilePath+ '" "' + @_PathHistorical + '"'
			--EXEC xp_cmdshell @cmd
		END
		ELSE BEGIN
			IF @_FileExists = 0 PRINT 'CarrierGEFCOFileSender >> Error >> File not created'	
		END		
		SET @ERR = 1
	END
FIN:

-- Retorna resultado de la ejecución.
RETURN @ERR
--FIN
