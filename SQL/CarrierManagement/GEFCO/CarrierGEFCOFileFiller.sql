﻿CREATE PROCEDURE [dbo].[CarrierGEFCOFileFiller]

--PARÁMETROS DE ENTRADA
	@CODHJA NVARCHAR (25) = NULL

--PARÁMETROS DE SALIDA
AS

--<sysCustomSpScript>
	DECLARE @ZCall_CarrierGEFCOFileFiller AS nvarchar(MAX); DECLARE @ZParam_CarrierGEFCOFileFiller AS nvarchar(MAX); DECLARE @ZExists_CarrierGEFCOFileFiller AS bit; DECLARE @Override AS BIT;
	SET @Override = 1;
	EXEC [dbo].[sysCustomSpGet] @SpId = 'CarrierGEFCOFileFiller', @CustomSpCall = @ZCall_CarrierGEFCOFileFiller OUTPUT, @CustomSpParam = @ZParam_CarrierGEFCOFileFiller OUTPUT, @CustomSpExists =  @ZExists_CarrierGEFCOFileFiller OUTPUT --, @UserId = @CODUSR
	IF (@ZExists_CarrierGEFCOFileFiller <> 0) BEGIN
		PRINT('Exectuing custom procedures >>> ' + @ZCall_CarrierGEFCOFileFiller);
		PRINT('	With parameters: ' + @ZParam_CarrierGEFCOFileFiller);
		EXEC sp_executesql @ZCall_CarrierGEFCOFileFiller, @ZParam_CarrierGEFCOFileFiller
			,@CODHJA = @CODHJA, @Override = @Override OUTPUT
		IF (ISNULL(@Override, 1) = 1)
			GOTO FIN;
	END
--</sysCustomSpScript>

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

CREATE TABLE #SetOrder (
	[STR]		nvarchar(2047),
	[NUMEXP]	nvarchar(25),
	[ROWID]		bigint identity(1,1)
)

--Record Header
INSERT INTO #SetOrder
SELECT TOP 1
	  'H1'								--Record header Identification*
	+ '787'								--Application*
	+ dbo.PadRight('XXXXXX','',35)		--Shipper Identification*
	+ dbo.PadRight('XXXXXX','',17)			--Account Number
	+ dbo.PadRight(VSH.[ShipFrom.CompanyName],'',35)	--Shipper’s Name
	+ dbo.PadRight(VSH.[ShipFrom.City],'',35)	--Shipper’s Town
	+ dbo.PadRight('XXXXXX','',35)			--GEFCO depot Identification*
	+ dbo.PadRight('BARCELONA','',35)			--GEFCO depot Town
	+ dbo.PadLeft('',0,8)				--Manifest number
	+ dbo.PadLeft('',0,2)				--Load Number
	+ dbo.PadLeft(CONVERT(NVARCHAR(25), ISNULL(C130FECEXP,GETDATE()), 112),0,8)			--Date of the document*
	+ dbo.PadLeft('',0,3)				--Number of shipments in the load
	+ dbo.PadLeft('',0,4)				--Number of packages
	+ dbo.PadLeft('',0,5)				--Total Gross weight
	+ dbo.PadLeft('',0,15)				--Total amount of Cash on delivery
	+ dbo.PadRight('','',3)				--Currency for Cash on delivery
	+ dbo.PadLeft('',0,11)				--Filler
	,VSH.ShipmentId
FROM Shipment
INNER JOIN V130UAS 
	ON C130CODALM = WarehouseId
	AND ShipmentId = C130ShipmentId
INNER JOIN VShipmentHeaders VSH
	ON VSH.WarehouseId = Shipment.WarehouseId
	AND VSH.ShipmentId = C130ShipmentId
WHERE ISNULL(C130CODHJA,RouteSheetId) = @CODHJA OR ISNULL(@CODHJA, N'') = N''

--Record Detail
INSERT INTO #SetOrder
SELECT DISTINCT
	  'RDE'						--Record Detail Identification*
	+ dbo.PadRight(SHP.ShipmentId,'',35)	--Shipper’s Shipment reference*
	+ dbo.PadLeft(ISNULL(C130CODHJA,RouteSheetId),0,8)		--Number of waybill (receipt or CMR)
	+ dbo.PadRight('1','',1)		--Indicator about receipt to print*
	+ dbo.PadRight('3','',1)		--Shipping conditions*
	+ dbo.PadRight('','',35)	--Consignee identification
	+ dbo.PadRight(ISNULL(VSH.[ShipTo.CompanyName],''),'',35)	--Consignee’s Name*
	+ dbo.PadRight(ISNULL(VSH.[ShipTo.Address],''),'',35)	--Consignee’s Address 1*
	+ dbo.PadRight(ISNULL(VSH.[ShipTo.Address2],''),'',35)	--Consignee’s Address 2
	+ dbo.PadRight(ISNULL(VSH.[ShipTo.ZipCode],''),'',9)		--ZIP code*
	+ dbo.PadRight(ISNULL(VSH.[ShipTo.City],''),'',35)	--Consignee’s Town*
	+ dbo.PadRight(ISNULL(VSH.[ShipTo.CountryISO2],''),'',2)		--Consignee’s Country Code*
	+ dbo.PadRight('','',2)		--Country code for final Consignee
	+ dbo.PadLeft('',0,25)		--Phone number
	+ dbo.PadRight('','',35)	--Contact’s Name for the delivery
	+ dbo.PadLeft(ISNULL(ShipmentPackagesTotal,0),0,4)		--Number of packages*
	+ dbo.PadLeft(ISNULL(ShipmentPackagesTotal,0),0,4)		--Number of handing units*
	+ dbo.PadLeft('',0,4)		--Number of returnable equipment*
	+ dbo.PadLeft(CAST(SUM(C130PESUA) AS DECIMAL(6,1)),0,5)		--Gross Weight*
	+ dbo.PadRight(ISNULL(C230TIPENV,''),'',3)		--Incoterms*
	+ dbo.PadLeft('',0,8)		--Requested delivery date
	+ dbo.PadLeft('',0,4)		--Requested hour for the beginning of the delivery
	+ dbo.PadLeft('',0,4)		--Requested hour for the end of the delivery
	+ dbo.PadRight('','',25)	--Goods description
	+ dbo.PadRight('','',1)		--Indicator if dangerous goods*
	+ dbo.PadRight('','',70)	--Delivery Instructions 
	+ dbo.PadRight('','',3)		--Unity of charge
	+ dbo.PadLeft('',0,9)		--Number of units to be taxed
	+ dbo.PadLeft(ISNULL(C230REEIMP,0),0,15)		--Cash on delivery amount
	+ dbo.PadRight('EUR','',3)		--Cash on delivery currency
	+ dbo.PadLeft('',0,15)		--Amount for assurance
	+ dbo.PadRight('','',3)		--Currency for Amount for assurance
	+ dbo.PadLeft('',0,15)		--Trade invoice amount
	+ dbo.PadRight('','',3)		--Currency for Trade invoice
	+ dbo.PadRight('','',3)		--Traffic Type
	+ dbo.PadRight('','',17)	--Account or analytic Number
	+ dbo.PadRight('','',35)	--Charge Code (code I)
	+ dbo.PadRight('','',35)	--Sender’s Identification
	+ dbo.PadRight('','',35)	--Sender’s Name
	+ dbo.PadRight('','',35)	--Sender’s Address 1
	+ dbo.PadRight('','',35)	--Sender’s Address 2
	+ dbo.PadRight('','',9)	--Sender’s ZIP code
	+ dbo.PadRight('','',35)	--Sender’s Town
	+ dbo.PadRight('','',2)	--Sender’s Country code
	+ dbo.PadLeft(CAST(SUM(C130VOLUA/1000000) AS DECIMAL(6,3)),0,6)		--Volume (m3)
	+ dbo.PadLeft(CAST(SUM(ISNULL(C130PROFUNDO,0)) AS DECIMAL(6,2)),0,6)		--Length (ml)	
	,SHP.ShipmentId
FROM [dbo].[Shipment] SHP
INNER JOIN V130UAS 
	ON C130CODALM = WarehouseId
	AND ShipmentId = C130ShipmentId
INNER JOIN T230ENTSAL
	ON C230CODALM = C230CODALM
	AND C230NUMENT = C130NUMENT
LEFT JOIN VShipmentHeaders VSH
	ON VSH.WarehouseId = SHP.WarehouseId
	AND VSH.ShipmentId = C130ShipmentId
OUTER APPLY (			
	SELECT TOP 1 *										--// cogemos la 1a que coincida
	FROM CarrierGEFCODestinations Carrier  
	WHERE 
			PostalCode LIKE '%' + VSH.[ShipTo.ZipCode] + '%'
		AND Carrier.CountryId = VSH.[ShipTo.CountryISO2]
	) AS GEFCO
WHERE ISNULL(C130CODHJA,RouteSheetId) = @CODHJA OR ISNULL(@CODHJA, N'') = N''
GROUP BY 
	 SHP.ShipmentId
	,C130CODHJA
	,RouteSheetId
	,VSH.[ShipTo.CompanyName]
	,VSH.[ShipTo.Address]
	,VSH.[ShipTo.Address2]
	,VSH.[ShipTo.ZipCode]
	,VSH.[ShipTo.City]
	,VSH.[ShipTo.CountryISO2]
	,ShipmentPackagesTotal
	,C230TIPENV
	,C230REEIMP

--Record Package
INSERT INTO #SetOrder
SELECT DISTINCT 
	'PCI'	--Identification of Record package*
	+ dbo.PadRight('00' + C130NUMUA,'',35) --Number of the barcode of shipper parcel*
	+ dbo.PadRight('','',35) --Barcode of a consolidation handling unit enclosing different packages (handling units) for one or several consignees.
	+ dbo.PadRight('','',55) --Filler
	,ShipmentId
FROM Shipment
INNER JOIN V130UAS 
	ON C130CODALM = WarehouseId
	AND ShipmentId = C130ShipmentId
WHERE ISNULL(C130CODHJA,RouteSheetId) = @CODHJA OR ISNULL(@CODHJA, N'') = N''

SELECT STR FROM #SetOrder ORDER BY NUMEXP, ROWID

FIN:
