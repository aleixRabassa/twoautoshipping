﻿CREATE PROCEDURE [dbo].[ShipmentLabeling_GEFCO]
--ENTRA
	 @WarehouseId	AS NVARCHAR(25)
	,@ShipmentId	AS NVARCHAR(25) = N''
	,@HUId			AS NVARCHAR(25)	= N''
--SALIDA
	,@ERR AS INT					= 1 OUTPUT
AS
--CONTROL
SET @ERR = 1

DECLARE @sysLanguageId AS NVARCHAR(5);

DECLARE @eFECSER AS NVARCHAR(100)
DECLARE @eHORENT AS NVARCHAR(100)
DECLARE @eATTENT AS NVARCHAR(100)
DECLARE @eTELORI AS NVARCHAR(100)
DECLARE @eTELDES AS NVARCHAR(100)
DECLARE @eFROM AS NVARCHAR(100)
DECLARE @eTO AS NVARCHAR(100)
DECLARE @eCARRIER AS NVARCHAR(100)
DECLARE @eNUMENT AS NVARCHAR(100)
DECLARE @eDOCCLI AS NVARCHAR(100)
DECLARE @eNUMDOC AS NVARCHAR(100)
DECLARE @ePESUA AS NVARCHAR(100)
DECLARE @eVOLBUL AS NVARCHAR(100)
DECLARE @eNUMBUL AS NVARCHAR(100)
DECLARE @eOBS1 AS NVARCHAR(100)
DECLARE @eOBS2 AS NVARCHAR(100)

DECLARE @DELS AS NVARCHAR(255)
DECLARE @DOCS AS NVARCHAR(255)
DECLARE @CUSDOCS AS NVARCHAR(255)
DECLARE @MUESAL AS NVARCHAR(255)
DECLARE @MINFECSER AS DATETIME

--// Ha de llegar Shipment o UA, sinó error
IF ISNULL(@ShipmentId, N'') = N'' AND ISNULL(@HUId, N'') = N'' BEGIN
	RAISERROR(N'UAoShipmentObligatorio',16,1);
	SET @ERR = -1;	
	GOTO FIN;
END

--// Si no llega Shipement se busca a partir de la UA.
IF ISNULL(@ShipmentId, N'') = N'' BEGIN
	SELECT TOP 1 @ShipmentId = C130ShipmentId FROM [dbo].[V130UAS] WHERE C130NUMUA = @HUId
END

--// Si no llega UA se busca a partir del Shipement
--IF ISNULL(@HUId, N'') = N'' BEGIN
--	SELECT TOP 1 @HUId = C130NUMUA FROM [dbo].[V130UAS] WHERE C130ShipmentId = @ShipmentId
--END


--// ************************************ LITERALES ETIQUETA **********************************************
--// ******************************************************************************************************
	
--Recuperamos idioma en función del país de destino
SELECT TOP 1
	@sysLanguageId = sysLanguageId 
FROM 
			[dbo].[Shipment] 
INNER JOIN 
			[dbo].T600PTOOPE 
		ON  C600CODENT = ShipTo
INNER JOIN 
			[dbo].Country 
		ON	C600CODPAI = CountryId
WHERE 
		ShipmentId = @ShipmentId

PRINT '@sysLanguageId: ' + @sysLanguageId
-- Recupera literales de etiqueta.

SET @eFECSER	= [dbo].[getSysControlFormText](N'eFECSER', @sysLanguageId, N'Fecha')
SET @eHORENT	= [dbo].[getSysControlFormText](N'eHORENT', @sysLanguageId, N'Hora')
SET @eATTENT	= [dbo].[getSysControlFormText](N'eATTENT', @sysLanguageId, N'Att.')
SET @eTELORI	= [dbo].[getSysControlFormText](N'eTELORI', @sysLanguageId, N'Tel.')
SET @eTELDES	= [dbo].[getSysControlFormText](N'eTELDES', @sysLanguageId, N'Tel.')
SET @eFROM		= [dbo].[getSysControlFormText](N'eFROM', @sysLanguageId, N'De:')
SET @eTO		= [dbo].[getSysControlFormText](N'eTO', @sysLanguageId, N'Para:')
SET @eCARRIER	= [dbo].[getSysControlFormText](N'eCARRIER', @sysLanguageId, N'Transporte')
SET @eNUMENT	= [dbo].[getSysControlFormText](N'eNUMENT', @sysLanguageId, N'Entrega')
SET @eDOCCLI	= [dbo].[getSysControlFormText](N'eDOCCLI', @sysLanguageId, N'Referencia')
SET @eNUMDOC	= [dbo].[getSysControlFormText](N'eNUMDOC', @sysLanguageId, N'Pedido')
SET @ePESUA		= [dbo].[getSysControlFormText](N'ePESUA', @sysLanguageId, N'Peso')
SET @eVOLBUL	= [dbo].[getSysControlFormText](N'eVOLBUL', @sysLanguageId, N'Volumen')
SET @eNUMBUL	= [dbo].[getSysControlFormText](N'eNUMBUL', @sysLanguageId, N'Bultos')
SET @eOBS1		= [dbo].[getSysControlFormText](N'eOBS1', @sysLanguageId, N'Obs')
SET @eOBS2		= [dbo].[getSysControlFormText](N'eOBS2', @sysLanguageId, N'Obs')

--// ************************************ LITERALES ETIQUETA **********************************************
--// ******************************************************************************************************
	SET @DELS = (SELECT DISTINCT C130NUMENT + ', ' FROM V130UAS WHERE C130ShipmentId = @ShipmentId AND C130NUMENT <> '' FOR XML PATH(''))
	SET @DOCS = (SELECT DISTINCT C130NUMDOC + ', ' FROM V130UAS WHERE C130ShipmentId = @ShipmentId AND C130NUMDOC <> '' FOR XML PATH(''))
	SET @CUSDOCS = (SELECT DISTINCT C200DOCCLI + ', ' FROM T240ENTSALLIN INNER JOIN T230ENTSAL	ON C230CODALM = C240CODALM
					AND C230NUMENT = C240NUMENT	AND C230ShipmentId = @ShipmentId INNER JOIN T200DOCSAL ON C200CODEMP = C240CODEMP 
					AND C200TIPDOC = C230TIPDOC	AND C200NUMDOC = C240NUMDOC FOR XML PATH(''))
	SET @MUESAL = (SELECT MAX(C120NOMUBI) FROM T230ENTSAL INNER JOIN T120UBI ON C120CODUBI = C230MUESAL WHERE C230ShipmentId = @ShipmentId)
	SET @MINFECSER = (SELECT MIN(C230FECSER) FROM T230ENTSAL WHERE C230ShipmentId = @ShipmentId FOR XML PATH(''))

	SELECT TOP 1
		 VSH.*
		,V130UAS.*	
		,CAST(CAST(C130VOLUA/1000000 AS decimal(18,2)) AS nvarchar) + ' m3' C130VOLUA2
		,CAST(CAST(C130PESUA AS decimal(18,2)) AS nvarchar) + ' Kg' C130PESUA2
		,CAST([C130ShipmentPackagesNo] AS NVARCHAR) + N'/' + CAST([C130ShipmentPackagesTotal] AS NVARCHAR) AS [ShipmentPackages]
		,SUBSTRING(@DELS,0,LEN(@DELS))			AS Deliveries
		,SUBSTRING(@DOCS,0,LEN(@DOCS))			AS Documents
		,SUBSTRING(@CUSDOCS,0,LEN(@CUSDOCS))	AS CustomerDocs
		,@MUESAL								AS OutDockName
		,CONVERT(nvarchar,CAST(GETDATE() AS date),103)	AS ServiceDate
		,TrackingNumber	
		,'' AS Coment
		--,'00' + C130NUMUA	AS NUMUACORTO
		,RIGHT(C130NUMUA, 10)	AS NUMUACORTO
		,C130NUMUA				AS NUMUA

		,N'00' + C130NUMUA		AS DMXUA
		,N'(00) ' + C130NUMUA	AS DMXUALabel
		,N'00' + C130NUMUA		AS [Barcode]
		,N'(00) ' + C130NUMUA	AS [BarcodeLabel]

		,DirectionalCode	[DirectionalCode]
		,BayCode			[BayCode]

		--// Literales de etiqueta
		,@eFECSER AS [eDay]
		,@eHORENT AS [eTime]
		,@eATTENT AS [eAtt]
		,@eTELORI AS [ePhoneFrom]
		,@eTELDES AS [ePhoneTo]
		,@eFROM AS [eFrom]
		,@eTO as [eTo]
		,@eCARRIER AS [eCarrier]
		,@eNUMENT as [eDeliveryId]
		,@eDOCCLI as [eCusOrderId]
		,@eNUMDOC as [eOrderId]
		,@ePESUA as [eWeight]
		,@eVOLBUL as [eVolume]
		,@eNUMBUL as [ePiece]
		,@eOBS1 as [eObs1]
		,@eOBS2 as [eObs2]

	FROM [dbo].[Shipment] Shipment
	LEFT JOIN [dbo].[V130UAS]
		ON  C130ShipmentId = ShipmentId
	LEFT JOIN VShipmentHeaders VSH
		ON VSH.WarehouseId = Shipment.WarehouseId
		AND VSH.ShipmentId = C130ShipmentId
	LEFT JOIN Country cd
		ON VSH.[ShipTo.CountryId] IN (cd.CountryId, cd.CountryISO2, cd.CountryName)
	OUTER APPLY (			
		SELECT TOP 1 *										--// cogemos la 1a que coincida
		FROM CarrierGEFCODestinations Carrier  
		WHERE 
				PostalCode LIKE '%' + VSH.[ShipTo.ZipCode] + '%'
			AND Carrier.CountryId = cd.CountryISO2
		) AS GEFCO
	WHERE 
			C130ShipmentId = @ShipmentId 
		AND (C130NUMUA = @HUId OR ISNULL(@HUID, N'') = N'')
		AND ISNULL([C130ContainerUnitId], N'') = N''		--// Si tiene SU se imprime sólo el SU
	ORDER BY
		[C130ShipmentPackagesNo]
	FIN:

--// Retorna status.
RETURN @ERR

--FIN
