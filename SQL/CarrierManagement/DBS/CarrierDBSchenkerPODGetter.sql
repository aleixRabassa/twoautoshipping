﻿CREATE PROCEDURE [dbo].[CarrierDBSchenkerPODGetter]

AS

DECLARE @_FileExists AS INT;
DECLARE @_DirectoryProperty AS TABLE (IsFile bit, IsDir bit, HasParent bit)
DECLARE @_DownloadedFileNames AS TABLE ([fileName] nvarchar(255))
DECLARE @SQL_BULK				as nvarchar(4000)
DECLARE @PODSourcePath			as nvarchar(255)
DECLARE @PODDestPath			as nvarchar(255)
DECLARE @PODServerFTP			as nvarchar(255)
DECLARE @PODUserFTP				as nvarchar(255)
DECLARE @PODPasswordFTP			as nvarchar(255)
DECLARE @PODPathHistorical		as nvarchar(255)
DECLARE @cmd					as nvarchar(4000)
DECLARE @Output					as nvarchar(4000)
DECLARE @FileName				as nvarchar(255)
DECLARE @ERR					int

CREATE TABLE #tmp (
	--[Transportista]					nvarchar(25) null,
	[NumeroReferenciaSGA]			nvarchar(25) null,
	[NumeroReferenciaDBSCH Spain]	nvarchar(25) null,
	[NumeroReferenciaDBSCH (STT)]	nvarchar(25) null,
	[Fecha Hora]					datetime null,
	[Evento]						nvarchar(25) null,
	[Razon]							nvarchar(25) null,
	[DescripcionEvento]				nvarchar(255) null,
	[LinkEvento]					nvarchar(255) null,
	--[img]							image null
)

--// cogemos datos
SELECT 
	 @PODServerFTP = PODServerFTP
	,@PODUserFTP = PODUserFTP
	,@PODPasswordFTP = PODPasswordFTP
	,@PODSourcePath = PODSourcePath
	,@PODDestPath = PODDestPath
	,@PODPathHistorical = PODPathHistorical
FROM CarrierDBSchenkerConfig

--SET @PODSourcePath = '"' + @PODSourcePath + '"'
--SET @PODDestPath = '"' + @PODDestPath + '"'
--SET @PODPathHistorical = '"' + @PODPathHistorical + '"'

PRINT 'Servidor FTP: ' + @PODServerFTP
PRINT 'Path origen: ' + @PODSourcePath
PRINT 'Path destino: ' + @PODDestPath
PRINT 'Path histórico: ' + @PODPathHistorical

--Descargamos ficheros
PRINT 'Descargando ficheros.'
EXEC [dbo].[SCM_ftp_Getfile]		
	 @FTPServer = @PODServerFTP
	,@FTPUser = @PODUserFTP
	,@FTPPWD = @PODPasswordFTP
	,@DestPath = @PODDestPath
	,@SourcePath = @PODSourcePath
	,@workdir = @PODDestPath
	,@output = @Output Output	
	,@fullCmd = @cmd Output	

PRINT 'Command: ' + @cmd

--// Obtenemos los nombres de los ficheros descargados
-- .txt
SET @cmd = 'dir ' + @PODDestPath + '\*.txt /b'
INSERT INTO  @_DownloadedFileNames(fileName)
EXEC xp_cmdshell @cmd 

--.csv
SET @cmd = 'dir ' + @PODDestPath + '\*.csv /b'
INSERT INTO  @_DownloadedFileNames(fileName)
EXEC xp_cmdshell @cmd

--.tif
SET @cmd = 'dir ' + @PODDestPath + '\*.tif /b'
INSERT INTO  @_DownloadedFileNames(fileName)
EXEC xp_cmdshell @cmd

--.edi
SET @cmd = 'dir ' + @PODDestPath + '\*.edi /b'
INSERT INTO  @_DownloadedFileNames(fileName)
EXEC xp_cmdshell @cmd

DELETE FROM @_DownloadedFileNames WHERE fileName IS NULL OR fileName = 'File Not Found'
SELECT * FROM @_DownloadedFileNames

--// Recorremos los ficheros descargados
PRINT 'Importando datos de los ficheros descargaods.'
DECLARE CURSOR_CRS1 CURSOR LOCAL FAST_FORWARD FOR
SELECT fileName FROM @_DownloadedFileNames
OPEN CURSOR_CRS1
FETCH NEXT FROM CURSOR_CRS1
INTO
		@FileName
WHILE (@@FETCH_STATUS = 0) 
BEGIN 
	IF (@FileName NOT LIKE 'File Not Found%')
	BEGIN
		PRINT 'Importando ' + @FileName
		-- .txt, .csv, .edi
		IF @FileName LIKE '%.txt%' OR @FileName LIKE '%.csv%' OR @FileName LIKE '%.edi%' 
		BEGIN
			SELECT @FileName
			SET @SQL_BULK = 
				'BULK INSERT #tmp FROM ''' + @PODDestPath + '\' + @FileName + ''' WITH
					(
					FIELDTERMINATOR = '';'',
					ROWTERMINATOR = ''\n''
					)'
			EXEC (@SQL_BULK)
			SET @ERR =  @@ERROR
		END

		--.tif
		IF @FileName LIKE '%.tif%' 
		BEGIN	
			IF EXISTS(SELECT TOP 1 1 FROM CarrierDBSchenkerPOD WHERE SUBSTRING(@FileName, 1, LEN(@FileName) - 4) IN ([NumeroReferenciaDBSCH (STT)], [NumeroReferenciaDBSCH Spain], NumeroReferenciaSGA) and Evento = 'DLV')
			BEGIN
				SET @SQL_BULK = '
					INSERT INTO CarrierPODImages (img)
					SELECT BulkColumn FROM Openrowset(Bulk ''' + @PODDestPath + '\' + @FileName + ''', Single_Blob) as img
					UPDATE CarrierDBSchenkerPOD
					SET imgId = SCOPE_IDENTITY()
					WHERE ''' + SUBSTRING(@FileName, 1, LEN(@FileName) - 4) + ''' IN ([NumeroReferenciaDBSCH (STT)], [NumeroReferenciaDBSCH Spain], NumeroReferenciaSGA)
						AND Evento = ''DLV'''
				SELECT @FileName
				EXEC (@SQL_BULK)			
				SET @ERR =  @@ERROR		
			END
			ELSE BEGIN
				PRINT 'Referencia no encontrada en la tabla de PODs'
				SET @ERR = -1
			END
		END	
		SELECT * FROM #tmp
		INSERT INTO CarrierDBSchenkerPOD 
		([NumeroReferenciaSGA],[NumeroReferenciaDBSCH Spain],[NumeroReferenciaDBSCH (STT)],[Fecha Hora],[Evento],[Razon],[DescripcionEvento],[LinkEvento]) 	
		SELECT [NumeroReferenciaSGA],[NumeroReferenciaDBSCH Spain],[NumeroReferenciaDBSCH (STT)],CONVERT(datetime, [Fecha Hora]),[Evento],[Razon],[DescripcionEvento],[LinkEvento] FROM #tmp
		TRUNCATE TABLE #tmp

		IF (@ERR = 0)
		BEGIN
			PRINT @FileName + ' importado.'

			--// Movemos ficheros procesados a la carpeta de histórico	
			PRINT 'Moviendo ' + @FileName + ' a historico.'
			SELECT @cmd = 'move "' + @PODDestPath + '\' + @FileName + '" "' + @PODPathHistorical + '"'
			EXEC xp_cmdshell @cmd
			PRINT 'Command: ' + @cmd
		END
		ELSE BEGIN
			PRINT @FileName + ' no importado.'
		END
	END

	FETCH NEXT FROM CURSOR_CRS1
	INTO
		@FileName
END
CLOSE CURSOR_CRS1;
DEALLOCATE CURSOR_CRS1;
