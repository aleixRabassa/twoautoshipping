﻿CREATE PROCEDURE [dbo].[CarrierDBSchenkerFileFiller]
--PARÁMETROS DE ENTRADA
	@CODHJA NVARCHAR (25) = NULL
--PARÁMETROS DE SALIDA
AS
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED



SELECT TOP 1
	N'@@CAB' + 
	[dbo].[PadRight](Identifier, ' ', 20) +
	N'EXPEDICIONES'
FROM
	[dbo].[CarrierDBSchenkerConfig]


SELECT  DISTINCT
--REMITENTE
	  [dbo].[PadRight](O.CompanyName, N' ', 40)					--//Remitente.Nombre
	+ [dbo].[PadRight](O.[Address], N' ', 40)					--//Remitente.Dirección
	+ SPACE(8)													--//Remitente.Numero
	+ [dbo].[PadRight](O.City, N' ', 40)						--//Remitente.Población
	+ [dbo].[PadRight](O.PostalCode, N' ', 6)					--//Remitente.Codigo Postal

	+ [dbo].[PadRight](C230CODCLI, N' ', 10)					--//Destinatario.Codigo
	+ [dbo].[PadRight]([dbo].[RemoveLineBreak](ISNULL(ISNULL(C600NOMPTO,C090NOMEMP),SPACE(40))), N' ', 40)	--//Destinatario.Nombre
	+ [dbo].[PadRight]([dbo].[RemoveLineBreak](ISNULL(C600DIRECC,SPACE(40))), N' ', 40)						--//Destinatario.Dirección
	+ SPACE(8)																								--//Destinatario.Numero
	+ [dbo].[PadRight]([dbo].[RemoveLineBreak](ISNULL(C600POBLAC,SPACE(27))), N' ', 40)						--//Destinatario.Población
	+ [dbo].[PadRight](REPLACE(REPLACE(ISNULL(C600CODPOS,SPACE(6)),N'-',N''),N' ',N''), N' ', 6)			--//Destinatario.Codigo Postal
	+ [dbo].[PadRight](ISNULL(ISNULL(CDBSCtry.CountryISO, CDBSCtry.CountryId),C600CODPAI), N' ', 3)															--//Destinatario.Codigo Postal
	+ SPACE(50)													--//Mercancía
	+ [dbo].[PadRight](CASE WHEN CDBSCtry.CountryId NOT IN (N'042',N'ES') THEN ISNULL(C600CODPOS,SPACE(10)) ELSE N'' END, N' ', 10) --// C.Postal No España
	+ SPACE(1)													--// SAR
	+ SPACE(1)													--// Reembolso pagado en Origen Destino
	+ SPACE(8)													--// Reservado
	+ [dbo].[PadRight](REPLACE(REPLACE(ISNULL(O.PhoneNr,SPACE(15)),N'-',N''),N' ',N''), N' ', 15)			--//Teléfono remitente
	+ [dbo].[PadRight](REPLACE(REPLACE(ISNULL(C600TELEFO,SPACE(15)),N'-',N''),N' ',N''), N' ', 15)			--//Teléfono destinatario
	+ [dbo].[PadLeft](CONVERT(NVARCHAR(25),ISNULL(C230NUMBUL,0)), N'0', 6)--//Bultos
	+ N'PAL'													--//Tipo de bulto
	+ [dbo].[PadLeft](CONVERT(NVARCHAR(25),CONVERT(INT,ROUND(ISNULL(C230PESBUL,0),0))), N'0', 9)	--//Peso
	+ [dbo].[PadLeft](CONVERT(NVARCHAR(25),CONVERT(INT,ROUND(ISNULL(C230VOLBUL,0)/1000,0))), N'0', 9)	--//Volumen
	+ [dbo].[PadRight](C230NUMENT, N' ', 15)					--// Ref.Cliente
	+ [dbo].[PadRight](LEFT(C.CustomerId, 2) + CAST(YEAR(ISNULL(C230FECEXP, GETDATE())) AS NVARCHAR(4)) + [dbo].[PadLeft](CONVERT(NVARCHAR(25),C230NUMEXP), N' ', 7), N' ', 15)	--//Ref. DB SCHENKER
	+ [dbo].[PadLeft](N'', N'0', 11)	--//Valor mercancía
	+ N'EUR'							--//Divisa valor
	+ [dbo].[PadRight](CST.CarrierShipmentType, N' ', 3)	--//Incoterms
	+ SPACE(58)							--//Condicionada
	+ N'N'								--//Tráfico
	+ SPACE(1)							--//Reservado
	+ [dbo].[PadLeft](N'', N'0', 11)	--//Importe condicionada
	+ N'EUR'							--//Divisa importe
	+ [dbo].[PadRight](ISNULL(C230OBSES,N''), N' ', 60)			--//Observaciones 1
	+ [dbo].[PadRight](ISNULL(C230OBSES1,N''), N' ', 60)			--//Observaciones 2
	+ CONVERT(VARCHAR(8), ISNULL(C230FECEXP, GETDATE()), 112)	--//Fecha salida
	+ [dbo].[PadLeft](CONVERT(NVARCHAR(25),CONVERT(INT,ROUND(ISNULL(C230REEIMP,0)*100,0))), N'0', 11)	--//Importe reembolso
	+ N'EUR'							--//Divisa reembolso
	+ N'N'								--//Seguro todo riesgo
	+ N'N'								--//ADR
	+ N'N'								--//GGSS
	+ SPACE(8)							--//Fecha entrega GGSS
	+ SPACE(4)							--//Hora entrega GGSS
	+ [dbo].[PadLeft](ISNULL(C600EMAIL, N''), N' ', 40)	--//Email destinatario
	
FROM [dbo].T230ENTSAL 
				INNER JOIN [dbo].T600PTOOPE ON C600CODENT=C230PTODES
				INNER JOIN [dbo].T090EMP ON C090CODENT=C230ENTCLI
				CROSS APPLY (
					SELECT TOP 1 * FROM [dbo].T240ENTSALLIN 
					INNER JOIN [dbo].V200DOC ON C200CODEMP=C240CODEMP AND C200TIPDOC=C240TIPDOC AND C200NUMDOC=C240NUMDOC
					LEFT OUTER JOIN T201DSOBS ON C201CODEMP=C200CODEMP AND C201TIPDOC=C200TIPDOC AND C201NUMDOC=C200NUMDOC
					WHERE C240CODALM=C230CODALM AND C240NUMENT=C230NUMENT) DOCS
				LEFT OUTER JOIN [dbo].DeliveryPackingHUs  ON WarehouseId = C230CODALM AND DeliveryId = C230NUMENT
				--LEFT OUTER JOIN [dbo].Country ON C600CODPAI = CountryISO2
				LEFT OUTER JOIN [dbo].[CarrierShippingCo] CSC ON [CarrierDelegationId] = C230CODTRA AND [CarrierCode] = N'DBSCHENKER'
				LEFT OUTER JOIN [dbo].[Carrier] C  ON  C.[CarrierCode] = CSC.[CarrierCode] AND C.CarrierId = CSC.CarrierId
				INNER JOIN [dbo].[Owners] O ON O.OwnerId = C240CODEMP
				INNER JOIN [dbo].[CarrierShipmentType] CST ON CST.ShipmentType = C230TIPENV AND CST.[CarrierCode] = CSC.[CarrierCode]
				LEFT JOIN [dbo].[CarrierDBSchenkerConfig] CDBSC ON CDBSC.CarrierDBSchenkerId = C.CarrierId
				LEFT JOIN [dbo].[CarrierDBSchenkerCountry] CDBSCtry ON  CDBSCtry.TWOCountryId = C600CODPAI

WHERE C230CODHJA = @CODHJA




SELECT 
	N'@@FIN' 