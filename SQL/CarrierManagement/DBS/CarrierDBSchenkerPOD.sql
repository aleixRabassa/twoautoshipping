﻿CREATE TABLE CarrierDBSchenkerPOD (
	[NumeroReferenciaSGA]			nvarchar(25) null,
	[NumeroReferenciaDBSCH Spain]	nvarchar(50) null,
	[NumeroReferenciaDBSCH (STT)]	nvarchar(50) null,
	[Fecha Hora]					datetime null,
	[Evento]						nvarchar(25) null,
	[Razon]							nvarchar(25) null,
	[DescripcionEvento]				nvarchar(255) null,
	[LinkEvento]					nvarchar(255) null,
	[FechaProceso]					datetime null,
	[CreationDate]					datetime default (getdate()) not null,	
	--[img]							image null,
	[imgId]						bigint null,
	[rowid]							bigint identity(1,1) not null
)