﻿CREATE PROCEDURE [dbo].[CMI_DBSchenkerPOD]
AS
	SELECT NumeroReferenciaSGA, CreationDate, 'Referencia no encontrada.' AS ErrorMessage
	FROM CarrierDBSchenkerPOD WHERE FechaProceso IS NULL
	ORDER BY 2 DESC
RETURN 0