﻿CREATE TABLE [dbo].[CarrierDBSchenkerConfig]
(
	[rowid]							BIGINT          IDENTITY (1, 1) NOT NULL,
	[rowver]						ROWVERSION      NOT NULL,
	[CreationDate]					DATETIME        CONSTRAINT [DF_CarrierDBSchenkerConfig_CreationDate] DEFAULT (getdate()) NOT NULL,
	[DeletionDate]					DATETIME		NULL,
	[LastEditDate]					DATETIME		NULL,
	[CarrierDBSchenkerId]			NVARCHAR(25)	CONSTRAINT [DF_CarrierDBSchenkerConfig_CarrierDachserId] DEFAULT (N'Default') NOT NULL,
	[Identifier]					NCHAR(20)		CONSTRAINT [DF_CarrierDBSchenkerConfig_Identifier] DEFAULT (N'') NOT NULL,
	[MailFrom]						NVARCHAR(255)	CONSTRAINT [DF_CarrierDBSchenkerConfig_MailFrom] DEFAULT (N'') NOT NULL,  --Email FROM
	[MailTo]						NVARCHAR(255)	CONSTRAINT [DF_CarrierDBSchenkerConfig_MailTo] DEFAULT (N'') NOT NULL,  	  --Lista de emails de Dachser
	[MailCC]						NVARCHAR(255)	CONSTRAINT [DF_CarrierDBSchenkerConfig_MailCC] DEFAULT (N'') NOT NULL,  --Lista de emails que recibirán copia
	[MailBCC]						NVARCHAR(255)	CONSTRAINT [DF_CarrierDBSchenkerConfig_MailBCC] DEFAULT (N'') NOT NULL,  --Lista de emails que recibirán copia oculta
	[MailSubject]					NVARCHAR(255)	CONSTRAINT [DF_CarrierDBSchenkerConfig_MailSubject] DEFAULT (N'') NOT NULL,
	[CarrierCode]					AS (CONVERT([nvarchar](25),N'DBSCHENKER',0)) PERSISTED,
	PODSourcePath					nvarchar(255)	null,	
	--PODSourceFileName				nvarchar(255)	null,
	PODDestPath						nvarchar(255)	null,
	--PODDestFileName					nvarchar(255)	null,
	PODServerFTP					nvarchar(25)	null,
	PODUserFTP						nvarchar(25)	null,
	PODPasswordFTP					nvarchar(25)	null,
	PODPathHistorical				nvarchar(255)	null,	
	CONSTRAINT [CarrierDBSchenkerConfig_PK] PRIMARY KEY CLUSTERED ([CarrierDBSchenkerId] ASC) WITH (FILLFACTOR = 90, PAD_INDEX = ON),
	CONSTRAINT [CarrierDBSchenkerConfig_FK_Carrier] FOREIGN KEY ([CarrierCode], [CarrierDBSchenkerId]) REFERENCES [dbo].[Carrier] ([CarrierCode], [CarrierId])
)
