﻿CREATE TABLE DBSchenkerDescriptions
(
	[Type]			NVARCHAR(25) NULL,
	[Code]			NVARCHAR(25) NULL,
	[Description]	NVARCHAR(255) NULL
)