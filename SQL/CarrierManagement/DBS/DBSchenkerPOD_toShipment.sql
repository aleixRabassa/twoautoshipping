﻿CREATE PROCEDURE [dbo].[DBSchenkerPOD_toShipment]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	DECLARE @DATE AS DATETIME
	SET @DATE = GETDATE()

	UPDATE CarrierDBSchenkerPOD SET FechaProceso = @DATE
	FROM T230ENTSAL 
	WHERE NumeroReferenciaSGA = C230NUMENT AND FechaProceso IS NULL

	UPDATE Shipment SET 
		 TrackingStatus = 'DB Schenker: ' + ISNULL(E.[Description],'') + ' ' + ISNULL(R.[Description],'')
		,ProofOfDelivery = CASE WHEN Evento = 'DLV' THEN 'T' ELSE ProofOfDelivery END
		,ArrivalDate = CASE WHEN Evento = 'DLV' THEN CarrierDBSchenkerPOD.[Fecha Hora] ELSE ArrivalDate END
	FROM Shipment
	INNER JOIN T230ENTSAL
		ON  WarehouseId = C230CODALM
		AND ShipmentId = C230ShipmentId
	INNER JOIN (			
		SELECT NumeroReferenciaSGA, MAX([Fecha Hora]) [Fecha Hora]			--// solo cogemos el último estado.
		FROM CarrierDBSchenkerPOD
		INNER JOIN DBSchenkerDescriptions
			ON Evento = Code
		GROUP BY NumeroReferenciaSGA
	) LastEvent
		ON LastEvent.NumeroReferenciaSGA = C230NUMENT
	INNER JOIN (
		SELECT * FROM CarrierDBSchenkerPOD
		INNER JOIN DBSchenkerDescriptions
			ON Evento = Code
	) CarrierDBSchenkerPOD 
		ON CarrierDBSchenkerPOD.NumeroReferenciaSGA = LastEvent.NumeroReferenciaSGA
		AND CarrierDBSchenkerPOD.[Fecha Hora] = LastEvent.[Fecha Hora]
	LEFT JOIN DBSchenkerDescriptions E	--// evento
		ON Evento = E.Code	
	LEFT JOIN DBSchenkerDescriptions R	--// razón
		ON Razon = R.Code	
	WHERE CarrierDBSchenkerPOD.FechaProceso = @DATE
END
