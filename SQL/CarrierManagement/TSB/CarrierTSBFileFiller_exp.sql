﻿CREATE PROCEDURE [dbo].[CarrierTSBFileFiller_exp]

--PARÁMETROS DE ENTRADA
	@CODHJA NVARCHAR (25) = NULL

--PARÁMETROS DE SALIDA
AS
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	--<sysCustomSpScript>
	DECLARE @ZCall_CarrierTSBFileFiller_exp AS nvarchar(MAX); DECLARE @ZParam_CarrierTSBFileFiller_exp AS nvarchar(MAX); DECLARE @ZExists_CarrierTSBFileFiller_exp AS bit; DECLARE @Override AS BIT;
	SET @Override = 1;
	EXEC [dbo].[sysCustomSpGet] @SpId = 'CarrierTSBFileFiller_exp', @CustomSpCall = @ZCall_CarrierTSBFileFiller_exp OUTPUT, @CustomSpParam = @ZParam_CarrierTSBFileFiller_exp OUTPUT, @CustomSpExists =  @ZExists_CarrierTSBFileFiller_exp OUTPUT --, @UserId = @CODUSR
	IF (@ZExists_CarrierTSBFileFiller_exp <> 0) BEGIN
		PRINT('Exectuing custom procedures >>> ' + @ZCall_CarrierTSBFileFiller_exp);
		PRINT('	With parameters: ' + @ZParam_CarrierTSBFileFiller_exp);
		EXEC sp_executesql @ZCall_CarrierTSBFileFiller_exp, @ZParam_CarrierTSBFileFiller_exp
			,@CODHJA = @CODHJA, @Override = @Override OUTPUT
		IF (ISNULL(@Override, 1) = 1)
			GOTO FIN;
	END
	--</sysCustomSpScript>


	SELECT DISTINCT
					ISNULL(TrackingNumber, N'')						--ReferenciaCliente*
		+ N'|' +	ISNULL(VSH.WarehouseId,N'')						--ClienteRemitente*
		+ N'|' +	'01'											--CentroRemitente*
		+ N'|' +	ISNULL(VSH.[ShipTo.CompanyName], N'')			--NombreDestinatario*
		+ N'|' +	ISNULL(VSH.[ShipTo.Address], N'')			--DireccionDestinatario*
		+ N'|' +	ISNULL(VSH.[ShipTo.CountryISO3], N'')			--PaisDestinatario*
		+ N'|' +	ISNULL(VSH.[ShipTo.ZipCode], N'')				--CodigoPostalDestinatario*
		+ N'|' +	ISNULL(VSH.[ShipTo.City], N'')			--PoblacionDestinatario*
		+ N'|' +	ISNULL('', N'')						--NifDestinatario
		+ N'|' +	ISNULL('', N'')						--PersonaContactoDestinatario
		+ N'|' +	ISNULL(VSH.[ShipTo.Phone], N'')			--TelefonoContactoDestinatario
		+ N'|' +	ISNULL(CAST(C130ShipmentPackagesTotal AS NVARCHAR), N'')		--Bultos*
		+ N'|' +	ISNULL(CAST(CAST(SUM(C130PESUA) AS INT) AS NVARCHAR), N'')			--Kilos*		
		+ N'|' +	ISNULL(CAST(CAST(SUM(C130VOLUA)/1000000 AS DECIMAL(6,3)) AS NVARCHAR), N'000.000')				--Volumen
		+ N'|' +	ISNULL(CASE WHEN ShipmentType IN ('000','CIP','CPT') THEN 'P' ELSE 'D' END, N'')			--TipoPortes*
		+ N'|' +	ISNULL('', N'')			--Observaciones
		+ N'|' +	ISNULL(C130NUMDOC, N'')			--NumeroPedido
		+ N'|' +	ISNULL('', N'')			--FechaEntregaAplazada
		+ N'|' +	ISNULL('', N'')			--FechaEntregaMaxima
		+ N'|' +	ISNULL('', N'')			--ReferenciaAgrupacion
		+ N'|' +	ISNULL('', N'')			--Localizador
		+ N'|' +	ISNULL('', N'')			--DevolucionConforme
		+ N'|' +	ISNULL('', N'')			--Identicket
		+ N'|' +	ISNULL('', N'')			--DepartamentoRemitente
		+ N'|' +	ISNULL('', N'')			--Referencia2
		+ N'|' +	ISNULL('', N'')			--FechaSalidaAplazada
		+ N'|' +	ISNULL(CAST(CAST(C230REEIMP AS DECIMAL(10,2)) AS NVARCHAR), N'')	--ImporteReembolso
		+ N'|' +	ISNULL('', N'')			--TipoPortesComisionReembolso
		+ N'|' +	ISNULL('', N'')			--ImporteValorDeclarado
		+ N'|' +	ISNULL('', N'')			--TKMailidio
		+ N'|' +	ISNULL('', N'')			--TKMail
		+ N'|' +	ISNULL('', N'')			--ExpedicionLiberada
		+ N'|' +	ISNULL('', N'')			--ImpresoraEtiquetas
	
	FROM [dbo].[Shipment] Shipment
	INNER JOIN [dbo].[V130UAS]
		ON  C130ShipmentId = Shipment.ShipmentId
	INNER JOIN T230ENTSAL							--// se necesita T230ENTSAL para recoger el CODHJA (en C130CODHJA y shp.RouteSheet no se informan)
		ON C230CODALM = C130CODALM
		AND C230NUMENT = C130NUMENT
	INNER JOIN VShipmentHeaders VSH
		ON VSH.WarehouseId = Shipment.WarehouseId
		AND VSH.ShipmentId = C130ShipmentId
	OUTER APPLY (
		SELECT TOP 1 *							--// cogemos la 1a que coincida
		FROM CarrierTSBDeliveryRoutes Carrier
		WHERE 
				PostalCode LIKE '%' + VSH.[ShipTo.ZipCode] + '%'
			AND Carrier.CountryId = VSH.[ShipTo.CountryISO3]
		) AS TSB
	WHERE ISNULL(C130CODHJA,RouteSheetId) = @CODHJA OR ISNULL(@CODHJA,'') = ''	
	GROUP BY
		 TrackingNumber
		,VSH.WarehouseId
		,VSH.[ShipTo.CompanyName]
		,VSH.[ShipTo.Address]
		,VSH.[ShipTo.CountryISO3]
		,VSH.[ShipTo.ZipCode]
		,VSH.[ShipTo.City]
		,VSH.[ShipTo.Phone]
		,C130ShipmentPackagesTotal
		,C130NUMDOC
		,ShipmentType
		,C230REEIMP

	FIN:
