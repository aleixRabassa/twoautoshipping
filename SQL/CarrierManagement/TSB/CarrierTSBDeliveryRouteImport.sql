﻿CREATE PROCEDURE [dbo].[CarrierTSBDeliveryRouteImport]
--ENTRA
	@ImportData dbo.[DeliveryRoutesTSB] READONLY,
	@ImportProfile NVARCHAR(25)
AS
	--CONTROL
	DECLARE @TRAN AS INT			--// Indicador de transacción abierta.


	SET XACT_ABORT ON;
	SET @TRAN = @@TRANCOUNT						--// Guarda si hay transacciones abiertas.
	IF @TRAN > 0 SAVE TRAN CarrierImport_TRA1	--// Guarda punto de recuperación si hay transacción abierta.
	ELSE BEGIN TRAN CarrierImport_TRA1			--// Abre transacción si no la hay.

	TRUNCATE TABLE [dbo].[CarrierTSBDeliveryRoutes]

	INSERT INTO [dbo].[CarrierTSBDeliveryRoutes] (CountryId, PostalCode, DeliveryRouteZone)
	SELECT DISTINCT
		 CountryId
		,PostalCode
		,DeliveryRouteZone
	FROM
		@ImportData

	IF @@ERROR < 0	ROLLBACK TRAN CarrierImport_TRA1
	ELSE IF @TRAN = 0 COMMIT TRAN CarrierImport_TRA1
RETURN 0