﻿CREATE TYPE [dbo].[DeliveryRoutesTSB] AS TABLE
(
     [CountryId]			NVARCHAR (25)
    ,[PostalCode]			NVARCHAR (25)
    ,[DeliveryRouteZone]	NVARCHAR (255)
)

