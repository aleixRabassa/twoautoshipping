﻿CREATE PROCEDURE [dbo].[CarrierTSBFileFiller_eti]

--PARÁMETROS DE ENTRADA
	@CODHJA NVARCHAR (25) = NULL

--PARÁMETROS DE SALIDA
AS
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT DISTINCT
					ISNULL(C230NUMEXP, N'')														--ReferenciaCliente*
		+ N'|' +	ISNULL(C230NUMEXP + dbo.[PadLeft](C130ShipmentPackagesNo,'0',3), N'')		--CodigoBarrasCliente*
		+ N'|' +	ISNULL(CAST(C130ShipmentPackagesTotal AS NVARCHAR), N'')					--NumeroBulto
		+ N'|' +	ISNULL(C130CODATV, N'')		--TipoBulto
		+ N'|' +	ISNULL('', N'')				--ReferenciaBultoCliente
		+ N'|' +	ISNULL('', N'')				--ModeloDescripcionBulto
		+ N'|' +	ISNULL(CAST(C130PROFUNDO AS NVARCHAR), N'')	--Largo
		+ N'|' +	ISNULL(CAST(C130ANCHO AS NVARCHAR),N'')		--Ancho
		+ N'|' +	ISNULL(CAST(C130ALTO AS NVARCHAR),N'')		--Alto
		+ N'|' +	ISNULL(CAST(C130VOLUA AS NVARCHAR),N'')		--Volumen
		+ N'|' +	ISNULL(CAST(C130PESUA AS NVARCHAR),N'')		--Kilos
		+ N'|' +	ISNULL('', N'')				--Observaciones
	
	FROM [dbo].[Shipment] Shipment
	INNER JOIN T230ENTSAL
		ON C230CODALM = WarehouseId
		AND C230ShipmentId = ShipmentId
	INNER JOIN [dbo].[V130UAS]
		ON  C130ShipmentId = Shipment.ShipmentId
	WHERE C230CODHJA = @CODHJA OR ISNULL(@CODHJA,'') = ''