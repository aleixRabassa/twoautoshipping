﻿CREATE PROCEDURE [dbo].[CarrierTSBFileFiller_adr]

--PARÁMETROS DE ENTRADA
	@CODHJA NVARCHAR (25) = NULL

--PARÁMETROS DE SALIDA
AS
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT DISTINCT
					ISNULL(C230NUMEXP, N'')											--ReferenciaCliente* 
		+ N'|' +	ISNULL(CAST(C130ShipmentPackagesTotal AS NVARCHAR), N'')		--NumeroBulto
		+ N'|' +	ISNULL('', N'')			--ONU*
		+ N'|' +	ISNULL('', N'')			--Descripción
		+ N'|' +	ISNULL('', N'')			--Clase
		+ N'|' +	ISNULL('', N'')			--GrupoEmbalaje
		+ N'|' +	ISNULL('', N'')			--TipoEnvase
		+ N'|' +	ISNULL('', N'')			--MaterialEnvase
		+ N'|' +	ISNULL('', N'')			--TipoEmbalaje
		+ N'|' +	ISNULL('', N'')			--MaterialEmbalaje
		+ N'|' +	ISNULL('', N'')			--NumeroBultos*
		+ N'|' +	ISNULL('', N'')			--PesoNeto
		+ N'|' +	ISNULL(CAST(C130PESUA AS NVARCHAR), N'')	--PesoBruto*
		+ N'|' +	ISNULL(CAST(C130VOLUA AS NVARCHAR), N'')	--Volumen
		+ N'|' +	ISNULL('', N'')			--VolumenLitros
		+ N'|' +	ISNULL('', N'')			--CodigoCatalogo
		+ N'|' +	ISNULL('', N'')			--Normativa
		+ N'|' +	ISNULL('', N'')			--PeligroMedioAmbiente
		+ N'|' +	ISNULL('', N'')			--Observaciones
	
	FROM [dbo].[Shipment] Shipment
	INNER JOIN T230ENTSAL
		ON C230CODALM = WarehouseId
		AND C230ShipmentId = ShipmentId
	INNER JOIN [dbo].[V130UAS]
		ON  C130ShipmentId = Shipment.ShipmentId
	--LEFT JOIN T600PTOOPE D
	--	ON ShipTo = D.C600CODENT
	--LEFT JOIN CarrierTSBDeliveryRoutes
	--	ON PostalCode = D.C600CODPOS
	WHERE C230CODHJA = @CODHJA OR ISNULL(@CODHJA,'') = ''