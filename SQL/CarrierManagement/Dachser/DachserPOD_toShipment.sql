﻿CREATE PROCEDURE [dbo].[DachserPOD_toShipment]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	DECLARE @DATE AS DATETIME
	SET @DATE = GETDATE()

	UPDATE CarrierDachserPOD SET NumeroReferenciaSGA = C230NUMENT, FechaProceso = @DATE
	FROM T230ENTSAL 
	WHERE SUBSTRING(NumeroReferenciaTransp,0,LEN(NumeroReferenciaTransp)-1) = C230NUMENT AND FechaProceso IS NULL


	UPDATE Shipment SET 
		 TrackingStatus = 'Dachser: Entregado.'
		,ProofOfDelivery = 'T'
		,ArrivalDate = CarrierDachserPOD.CreationDate
	FROM CarrierDachserPOD
	INNER JOIN T230ENTSAL
		ON SUBSTRING(NumeroReferenciaTransp,0,LEN(NumeroReferenciaTransp)-1) = C230NUMENT
	INNER JOIN Shipment
		ON  WarehouseId = C230CODALM
		AND ShipmentId = C230ShipmentId
		AND FechaProceso = @DATE

END
