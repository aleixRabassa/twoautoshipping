﻿CREATE PROCEDURE [dbo].[CarrierDachserFacilityImport]
--ENTRA
	@ImportData dbo.[FacilitiesDachser] READONLY,
	@ImportProfile NVARCHAR(25)
AS
	--CONTROL
	DECLARE @TRAN AS INT			--// Indicador de transacción abierta.


	SET XACT_ABORT ON;
	SET @TRAN = @@TRANCOUNT						--// Guarda si hay transacciones abiertas.
	IF @TRAN > 0 SAVE TRAN CarrierImport_TRA1	--// Guarda punto de recuperación si hay transacción abierta.
	ELSE BEGIN TRAN CarrierImport_TRA1			--// Abre transacción si no la hay.

	--TRUNCATE TABLE [dbo].[CarrierDachserFacilities]

	INSERT INTO [dbo].[CarrierDachserFacilities] ([FullRegister],	[CountryId], [FromPostalCode], [ToPostalCode], [DestinationFacilityCode], [DestinationName])
	SELECT DISTINCT
		  Facility						--// Full Register
		 ,SUBSTRING(Facility, 1, 2)		--// Country Code
		 ,SUBSTRING(Facility, 3, 5)		--// ZIP Start
		 ,SUBSTRING(Facility, 8, 5)		--// ZIP End
		 ,SUBSTRING(Facility, 13, 3)	--// Dachser depot code
		 ,SUBSTRING(Facility, 16, 12)	--// Dachser depot name
	FROM
		@ImportData

	IF @@ERROR < 0	ROLLBACK TRAN CarrierImport_TRA1
	ELSE IF @TRAN = 0 COMMIT TRAN CarrierImport_TRA1
RETURN 0