﻿CREATE PROCEDURE [dbo].[CarrierDachserFileFiller40]
--PARÁMETROS DE ENTRADA
	@CODHJA NVARCHAR (25) = NULL
--PARÁMETROS DE SALIDA
AS
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

--<sysCustomSpScript>
	DECLARE @ZCall_CarrierDachserFileFiller40 AS nvarchar(MAX); DECLARE @ZParam_CarrierDachserFileFiller40 AS nvarchar(MAX); DECLARE @ZExists_CarrierDachserFileFiller40 AS bit; DECLARE @Override AS BIT;
	SET @Override = 1;
	EXEC [dbo].[sysCustomSpGet] @SpId = 'CarrierDachserFileFiller40', @CustomSpCall = @ZCall_CarrierDachserFileFiller40 OUTPUT, @CustomSpParam = @ZParam_CarrierDachserFileFiller40 OUTPUT, @CustomSpExists =  @ZExists_CarrierDachserFileFiller40 OUTPUT --, @UserId = @CODUSR
	IF (@ZExists_CarrierDachserFileFiller40 <> 0) BEGIN
		PRINT('Exectuing custom procedures >>> ' + @ZCall_CarrierDachserFileFiller40);
		PRINT('	With parameters: ' + @ZParam_CarrierDachserFileFiller40);
		EXEC sp_executesql @ZCall_CarrierDachserFileFiller40, @ZParam_CarrierDachserFileFiller40
			,@CODHJA = @CODHJA, @Override = @Override OUTPUT
		IF (ISNULL(@Override, 1) = 1)
			GOTO FIN;
	END
--</sysCustomSpScript>

SELECT  DISTINCT
/*--------------------------------------TRS-------------------------------------------------------------*/
--IDENTIFICATION
	  'TRS006'														--//RECTYPE
	+ 'SCN_02'														--//ESCENARIO
	+ LEFT([dbo].[PadLeft](TrackingNumber, '0', 25) + SPACE(25), 25)--//TRSID
	+ CONVERT(VARCHAR(8), C230FECEXP, 112)							--//FECHA
	+ RIGHT(SPACE(6) + [dbo].[PadLeft](TrackingNumber, '0', 6), 6)	--//EXPEDICION
	+ LEFT([dbo].[PadLeft](LabelCounterCode, '0', 4) + [dbo].[PadLeft](C230NUMEXP, '0', 9) + SPACE(13), 13)		--//NUM_UNICO
	+ LEFT(C230NUMENT + SPACE(25), 25)							--//REFCLIENTE
	+ LEFT(C200NUMDOC + SPACE(25), 25)							--//REFCLIENTE2
--CONSIGNOR
	+ LEFT(ISNULL(CSC.CustomerId, '') + SPACE(9), 9)			--//RMT_CODIGO
	+ LEFT(O.CompanyName + SPACE(32), 32)						--//RMT_NOMBRE
	+ LEFT(SUBSTRING(O.CompanyName, 33, 30) + SPACE(30), 30)	--//RMT_NOMBRE2
	+ LEFT(SUBSTRING(O.CompanyName, 63, 30) + SPACE(30), 30)	--//RMT_NOMBRE3
	+ LEFT(O.[Address] + SPACE(40), 40)							--//RMT_DIRECCION
	+ LEFT(SUBSTRING(O.[Address], 41, 30) + SPACE(30), 30)		--//RMT_DIRECCION2
	+ LEFT(SUBSTRING(O.[Address], 71, 30) + SPACE(30), 30)		--//RMT_DIRECCION3
	+ LEFT(O.CountryCode + SPACE(2), 2)							--//RMT_PAIS
	+ LEFT(O.PostalCode + SPACE(12), 12)						--//RMT_POSTAL
	+ LEFT(O.City + SPACE(40), 40)								--//RMT_POBLACION
	+ LEFT(O.VATNr + SPACE(12), 12)								--//RMT_CIFDNI
	+ LEFT(ISNULL(O.EMAIL, N'') + SPACE(80), 80)				--//RMT_MAIL
	+ LEFT(ISNULL(O.FaxNr, N'') + SPACE(15), 15)				--//RMT_FAX
	+ LEFT(ISNULL(O.PhoneNr, N'') + SPACE(15), 15)				--//RMT_TELEFONO
	+ LEFT('' + SPACE(40), 40)									--//RMT_CONTACTO
	+ LEFT('' + SPACE(40), 40)									--//RMT_OBSERV
--CONSIGNEE
	+ LEFT('' + SPACE(9), 9)									--//CNS_CODIGO
	+ LEFT([dbo].[RemoveLineBreak](ISNULL(ISNULL(PTODES.C600NOMPTO,C090NOMEMP),SPACE(32))) + SPACE(32), 32)						--//CNS_NOMBRE
	+ LEFT(SUBSTRING([dbo].[RemoveLineBreak](ISNULL(ISNULL(PTODES.C600NOMPTO,C090NOMEMP),SPACE(32))), 33, 30) + SPACE(30), 30)	--//CNS_NOMBRE2
	+ LEFT([dbo].[RemoveLineBreak](ISNULL(ISNULL(PTOENT.C600NOMPTO, PTODES.C600NOMSUC),SPACE(30))) + SPACE(30), 30)				--//CNS_NOMBRE3
	+ LEFT([dbo].[RemoveLineBreak](ISNULL(PTODES.C600DIRECC,SPACE(40))) + SPACE(40), 40)										--//CNS_DIRECCION
	+ LEFT(SUBSTRING([dbo].[RemoveLineBreak](ISNULL(PTODES.C600DIRECC,SPACE(40))), 41, 30) + SPACE(30), 30)						--//CNS_DIRECCION2
	+ LEFT(SUBSTRING([dbo].[RemoveLineBreak](ISNULL(PTODES.C600DIRECC,SPACE(40))), 71, 30) + SPACE(30), 30)						--//CNS_DIRECCION3
	+ LEFT(CDF.CountryAlias + SPACE(2), 2)																						--//CNS_PAIS
	+ LEFT(REPLACE(REPLACE(ISNULL(PTODES.C600CODPOS,SPACE(12)),N'-',N''),N' ',N'') + SPACE(12), 12)								--//CNS_POSTAL
	+ LEFT ([dbo].[RemoveLineBreak](ISNULL(PTODES.C600POBLAC,SPACE(40))) + SPACE(40), 40)										--//CNL_POBLACION
	+ LEFT(CDF.[DestinationFacility] + SPACE(3), 3)																				--//CNS_PLAZA
	+ LEFT(ISNULL(C090CIFEMP, N'') + SPACE(12), 12)																							--//CNS_CIFDNI
	+ LEFT(ISNULL(PTODES.C600EMAIL, N'') + SPACE(80), 80)																		--//CNS_MAIL
	+ LEFT(ISNULL(PTODES.C600FAX, N'') + SPACE(15), 15)																			--//CNS_FAX
	+ LEFT(ISNULL(PTODES.C600TELEFO, N'') + SPACE(15), 15)																		--//CNS_TELEFONO
	+ LEFT('' + SPACE(40), 40)																									--//CNS_CONTACTO
	+ LEFT(ISNULL(PTODES.C600OBSER0,N'') + SPACE(40), 40)																		--//CNS_OBSERV
--ISSUER
	+ LEFT('' + SPACE(9), 9)									--//ORD_CODIGO
	+ LEFT('' + SPACE(40), 40)									--//ORD_NOMBRE
	+ LEFT('' + SPACE(12), 12)									--//ORD_CIFDNI
	+ LEFT('' + SPACE(80), 80)									--//ORD_MAIL
	+ LEFT('' + SPACE(15), 15)									--//ORD_FAX
	+ LEFT('' + SPACE(15), 15)									--//ORD_TELEFONO
	+ LEFT('' + SPACE(40), 40)									--//ORD_CONTACTO
	+ LEFT('' + SPACE(40), 40)									--//ORD_OBSERV
--GOODS
	+ RIGHT('0000'+CONVERT(NVARCHAR(25),Shipment.ShipmentPackagesTotal), 4)			--//BULTOS
	+ RIGHT('00000'+CONVERT(NVARCHAR(25),CONVERT(INT,ROUND(C230PESBUL,0))),5)		--//KILOS
	+ LEFT('00,000'+ SPACE(6), 6)								--//VOLUMEN
	+ LEFT('000' + SPACE(3), 3)									--//TB1
	+ LEFT('000' + SPACE(3), 3)									--//TB2
	+ LEFT('000' + SPACE(3), 3)									--//TB3
	+ LEFT('000' + SPACE(3), 3)									--//TB4
	+ LEFT('000' + SPACE(3), 3)									--//TB5
	+ LEFT('000' + SPACE(3), 3)									--//TB6
	+ LEFT('1' + SPACE(1), 1)									--//TIPOLOGIA
--CHARGES
	+ LEFT(CarrierShipmentType + SPACE(1), 1)					--//T_DESEMB ??
	+ LEFT(CarrierShipmentType + SPACE(1), 1)					--//T_PORTES ??
	+ LEFT(CarrierShipmentType + SPACE(1), 1)					--//T_REEXPED ??
	+ LEFT(CarrierShipmentType + SPACE(1), 1)					--//T_CREEMB ??
	+ RIGHT(REPLACE('0000000000'+CONVERT(VARCHAR,CAST(ROUND((ISNULL([C230REEIMP], 0)), 0) AS DECIMAL(7,2))),N'.',N','),10)	--//REEMBOLSO
	+ LEFT('0000000,00' + SPACE(10), 10)						--//VALSEG
	+ LEFT('' + SPACE(24), 24)									--//TXTSEG
	+ LEFT('' + SPACE(4), 4)									--//TIPMERC
	+ RIGHT('000' + ISNULL(DefaultProductCode, N''), 3)			--//PRODUCTO
	+ CASE WHEN DefaultProductCode = N'36' THEN CONVERT(VARCHAR(8), C230FECSER, 112) ELSE SPACE(8) END			--//FECHA_EA --> Obligatorio cuando DefaultProductCode = 36
	+ LEFT('' + SPACE(38), 38)									--//OBSERVACIONES1
	+ LEFT('' + SPACE(38), 38)									--//OBSERVACIONES2
	+ LEFT('' + SPACE(20), 20)									--//NUMEROFACTURA
	+ LEFT('' + SPACE(12), 12)									--//VALORFACTURA
	+ LEFT('' + SPACE(8), 8)									--//FECHAFACTURA
	+ LEFT('' + SPACE(15), 15)									--//EKAER
--ADDITIONAL SERVICES
	+ LEFT('' + SPACE(1), 1)									--//SA_IDENTICKET
	+ LEFT('' + SPACE(1), 1)									--//SA_ENTREGA_CONTRA_DOCUMENTO
	+ LEFT('' + SPACE(1), 1)									--//SA_ENTREGA_DOMICILIARIA
	+ LEFT('' + SPACE(1), 1)									--//SA_DIG_ALB_REMITENTE
	+ LEFT('' + SPACE(1), 1)									--//SA_DEV_ALB_REMITENTE
	+ LEFT('' + SPACE(1), 1)									--//SA_DIG_ACTA_RECEPCION
	+ LEFT('' + SPACE(1), 1)									--//SA_DEV_ACTA_RECEPCION
	+ LEFT('' + SPACE(1), 1)									--//SA_REQUIERE_DNI
	+ LEFT('' + SPACE(1), 1)									--//SA_ENTREGA_DIRECTA
	+ LEFT('' + SPACE(1), 1)									--//SA_NO_ADMITE_ENTREGA_PARCIAL
	+ LEFT('' + SPACE(1), 1)									--//SA_IMPRESCINDIBLE_CONCERTAR
	+ LEFT('' + SPACE(1), 1)									--//SA_SEGUNDO_CONDUCTOR
	+ LEFT('' + SPACE(1), 1)									--//SA_NOTIFICACION_ENTREGA
	+ LEFT('' + SPACE(1), 1)									--//SA_PLATAFORMA_ELEVADORA
	+ LEFT('' + SPACE(1) ,1)									--//SA_RECOGIDA_RAEE
	+ LEFT('' + SPACE(40),40)									--//SA_FILLER
--FIN
	+ '#'														--//MARCA_FIN_REC

FROM [dbo].T230ENTSAL 
				INNER JOIN [dbo].[DeliveryPackingHUs]	DP
					ON  DP.WarehouseId = C230CODALM
					AND DP.DeliveryId  = C230NUMENT
				INNER JOIN [dbo].[V130UAS]
					ON  C130NUMUA = DP.HUId
					AND (C130FECEXP = Dp.ShippingDate OR (C130FECEXP IS NULL AND DP.ShippingDate IS NULL))
				INNER JOIN [dbo].Shipment
					ON  Shipment.ShipmentId = ISNULL(DP.ShipmentId, ISNULL(C130ShipmentId, C230ShipmentId)) 
				INNER JOIN [dbo].T600PTOOPE PTODES ON PTODES.C600CODENT=C230PTODES
				LEFT JOIN [dbo].T600PTOOPE PTOENT ON PTOENT.C600CODENT=C230PTOENT
				INNER JOIN [dbo].T090EMP ON C090CODENT=C230ENTCLI
				CROSS APPLY (
					SELECT TOP 1 * FROM [dbo].T240ENTSALLIN 
					INNER JOIN [dbo].V200DOC ON C200CODEMP=C240CODEMP AND C200TIPDOC=C240TIPDOC AND C200NUMDOC=C240NUMDOC
					LEFT OUTER JOIN T201DSOBS ON C201CODEMP=C200CODEMP AND C201TIPDOC=C200TIPDOC AND C201NUMDOC=C200NUMDOC
					WHERE C240CODALM=C230CODALM AND C240NUMENT=C230NUMENT) DOCS
				LEFT OUTER JOIN [dbo].OutDeliveryPackage  ON C130CODALM = C230CODALM AND C130NUMENT = C230NUMENT
				--LEFT OUTER JOIN [dbo].Country ON PTODES.C600CODPAI = CountryISO2
				LEFT OUTER JOIN [dbo].[CarrierShippingCo] CSC ON CSC.CarrierDelegationId = C230CODTRA AND [CarrierCode] = N'DACHSER'
				LEFT OUTER JOIN [dbo].[Carrier] C  ON  C.[CarrierCode] = CSC.[CarrierCode] AND C.CarrierId = CSC.CarrierId
				LEFT OUTER JOIN [dbo].[CarrierDachserCountry] CAC ON PTODES.C600CODPAI = CAC.TWOCountryId
				LEFT OUTER JOIN [dbo].[CarrierDachserFacilities] CAP ON CAP.CountryId = CAC.CountryId AND PTODES.C600CODPOS BETWEEN CAP.FromPostalCode AND CAP.ToPostalCode
				INNER JOIN [dbo].[Owners] O ON O.OwnerId = C240CODEMP
				INNER JOIN [dbo].[CarrierShipmentType] CST ON CST.ShipmentType = C230TIPENV AND CST.[CarrierCode] = CSC.[CarrierCode]
				OUTER APPLY [dbo].[CarrierDachserFacility](PTODES.C600CODPOS,PTODES.C600CODPAI,NULL) CDF
				LEFT JOIN [dbo].[CarrierDachserConfig] CDC ON
						CDC.CarrierDachserId = C.CarrierId

WHERE C230CODHJA = @CODHJA
--ORDER BY
--		 C230NUMENT


SELECT  DISTINCT
/*--------------------------------------BTO-------------------------------------------------------------*/
--IDENT
	  'BTO002'																						--//RECTYPE
	+ LEFT([dbo].[PadLeft](TrackingNumber, '0', 25) + SPACE(25), 25)								--//TRSID
--REF
	+ LEFT('' + SPACE(50), 50)																		--//REFBTO
	+ LEFT(RIGHT([dbo].[CarrierDachserSSCC](CSC.CarrierId, UA.C130NUMUA),20) + SPACE(20), 20)			--//SSCC
	+ LEFT([dbo].[PadLeft](CONVERT(NVARCHAR(25),UA.C130ShipmentPackagesNo), '0', 3) + SPACE(3), 3)	--//NUMBTO
--MEDIDAS
	+ CASE WHEN UA.C130PESUA>9999.999 THEN 
			RIGHT(REPLACE('00000000'+CONVERT(VARCHAR,CAST(9999.999 AS DECIMAL(7,3))),N'.',N','),8)
		ELSE
			RIGHT(REPLACE('00000000'+CONVERT(VARCHAR,CAST((ISNULL(UA.C130PESUA, 0)) AS DECIMAL(7,3))),N'.',N','),8)	END	--//PESO
	+ RIGHT(REPLACE('00000000'+CONVERT(VARCHAR,CAST((ISNULL(UA.C130ANCHO/100, 0)) AS DECIMAL(5,3))),N'.',N','),6)		--//ANCHO
	+ RIGHT(REPLACE('00000000'+CONVERT(VARCHAR,CAST((ISNULL(UA.C130ALTO/100, 0)) AS DECIMAL(5,3))),N'.',N','),6)		--//ALTO
	+ RIGHT(REPLACE('00000000'+CONVERT(VARCHAR,CAST((ISNULL(UA.C130PROFUNDO/100, 0)) AS DECIMAL(5,3))),N'.',N','),6)	--//LARGO
--FIN
	+ '#'														--//MARCA_FIN_REC


FROM [dbo].T230ENTSAL 
				INNER JOIN [dbo].T600PTOOPE ON C600CODENT=C230PTODES
				INNER JOIN [dbo].T090EMP ON C090CODENT=C230ENTCLI
				CROSS APPLY (
					SELECT TOP 1 * FROM [dbo].T240ENTSALLIN 
					INNER JOIN [dbo].V200DOC ON C200CODEMP=C240CODEMP AND C200TIPDOC=C240TIPDOC AND C200NUMDOC=C240NUMDOC
					LEFT OUTER JOIN T201DSOBS ON C201CODEMP=C200CODEMP AND C201TIPDOC=C200TIPDOC AND C201NUMDOC=C200NUMDOC
					WHERE C240CODALM=C230CODALM AND C240NUMENT=C230NUMENT) DOCS
				LEFT OUTER JOIN [dbo].[DeliveryPackingHUs]  ON WarehouseId = C230CODALM AND DeliveryId = C230NUMENT
				--LEFT OUTER JOIN [dbo].Country ON C600CODPAI = CountryISO2
				LEFT OUTER JOIN [dbo].[CarrierShippingCo] CSC ON [CarrierDelegationId] = C230CODTRA AND [CarrierCode] = N'DACHSER'
				LEFT OUTER JOIN [dbo].[Carrier] C  ON  C.[CarrierCode] = CSC.[CarrierCode] AND C.CarrierId = CSC.CarrierId
				LEFT OUTER JOIN [dbo].[CarrierDachserCountry] CAC ON C600CODPAI = CAC.TWOCountryId
				LEFT OUTER JOIN [dbo].[CarrierDachserFacilities] CAP ON CAP.CountryId = CAC.CountryId AND C600CODPOS BETWEEN CAP.FromPostalCode AND CAP.ToPostalCode
				INNER JOIN [dbo].[Owners] O ON O.OwnerId = C240CODEMP
				INNER JOIN [dbo].[CarrierShipmentType] CST ON CST.ShipmentType = C230TIPENV
				INNER JOIN [dbo].[DeliveryPackingHUs] DP ON  DP.WarehouseId = C230CODALM AND DP.DeliveryId  = C230NUMENT
				INNER JOIN [dbo].[V130UAS] UA ON UA.C130NUMUA = DP.HUId
				LEFT JOIN [dbo].[V130UAS] CU
					ON  CU.C130NUMUA = UA.C130ContainerUnitId
				INNER JOIN [dbo].Shipment
					ON  Shipment.ShipmentId = ISNULL(DP.ShipmentId, ISNULL(UA.C130ShipmentId, C230ShipmentId)) 
				OUTER APPLY [dbo].[CarrierDachserFacility](C600CODPOS,C600CODPAI,NULL) CDF


WHERE C230CODHJA = @CODHJA AND (ISNULL(UA.C130ContainerUnitId, N'') = N'' OR CU.C130TIPUA <> N'SU')
--ORDER BY TrackingNumber, UA.C130ShipmentPackagesNo

FIN:
RETURN 0
