﻿CREATE PROCEDURE [dbo].[CarrierDachserFileSender]
	--ENTRA
	@CODHJA NVARCHAR (25) = NULL
	--SALIDA

AS
	--VARIABLES DE CONTROL
	DECLARE @_FullFilePath AS NVARCHAR(512);
	DECLARE @_FullFilePath2 AS NVARCHAR(512);
	DECLARE @_PathBinaries AS NVARCHAR(512);
	DECLARE @_FileExists AS INT;
	DECLARE @_DirectoryProperty AS TABLE (IsFile bit, IsDir bit, HasParent bit)
	DECLARE @ERR	AS INT				-- Retorno de resolución de funciones.
	DECLARE @FECENV AS NVARCHAR(6)		-- Fecha Entrega
	DECLARE @_EXEC AS NVARCHAR(255);
	DECLARE @_Path AS NVARCHAR(255);
	DECLARE @cmd AS NVARCHAR(1000);
	DECLARE @CODTRA AS NVARCHAR(25)
	DECLARE @_DachserMailFrom AS NVARCHAR(255)
	DECLARE @_SmtpServer AS NVARCHAR(255)
	DECLARE @_SmtpPort AS NVARCHAR(10)
	DECLARE @_LogonUser AS NVARCHAR(255)
	DECLARE @_LogonPassword AS NVARCHAR(255)
	DECLARE @_SmtpEncryption AS CHAR(1)
	DECLARE @_DachserMailTo AS NVARCHAR(255)										
	DECLARE @_DachserMailCC AS NVARCHAR(255)	
	DECLARE @_MailSubject AS NVARCHAR(255)
	DECLARE @ProcessId AS UNIQUEIDENTIFIER	

	--INICIO
	SET @ProcessId = NEWID()
	SET @ERR = 1
	IF EXISTS (SELECT TOP 1 1 FROM [dbo].[CarrierShippingCo] INNER JOIN [dbo].T230ENTSAL ON C230CODTRA = [CarrierDelegationId] WHERE C230CODHJA = @CODHJA AND [CarrierCode] = N'DACHSER') BEGIN --//Transportista DACHSER

		SELECT  
			@_PathBinaries = PathBinaries,
			@_Path = PathFiles
		FROM
			dbo.CarrierDachserConfig
		WHERE
			CarrierCode = 'DACHSER'

		SET @_EXEC=N'[dbo].[CarrierDachserFileFiller40] @CODHJA=' + N'''' + @CODHJA + N''''

		SELECT TOP 1 @CODTRA = C230CODTRA FROM [dbo].[T230ENTSAL] WHERE C230CODHJA = @CODHJA
		-- Generamos el nombre del fichero
		SET @_Path = @_Path + @CODHJA + '_'
	

		SELECT @_FullFilePath = @_Path + N'DACHSER_' + @CODTRA + N'_' + REPLACE(REPLACE(convert(varchar(20),GETDATE(),2)+convert(varchar(20),GETDATE(),108),'.',''),':','') + N'.txt'

		--// Copia de seguridad interna.
		EXECUTE SCM_BulkFile
		 @_FilePath = @_FullFilePath
		,@_SqlCommand = @_EXEC

		INSERT INTO @_DirectoryProperty EXEC master.[dbo].xp_fileexist @_FullFilePath
		SELECT @_FileExists = IsFile FROM @_DirectoryProperty
		IF (@_FileExists = 1) BEGIN
		--Si hemos generado el fichero lo enviaremos por mail
			SET @_FullFilePath2 = REPLACE(@_FullFilePath,N'.txt',N'_ANSI.txt') 
			SELECT @cmd = @_PathBinaries + N'UTF2ANSI.exe "' + @_FullFilePath + '" "' + @_FullFilePath2 +'"'
			EXEC xp_cmdshell @cmd
			INSERT INTO @_DirectoryProperty EXEC master.[dbo].xp_fileexist @_FullFilePath2
			SELECT @_FileExists = IsFile FROM @_DirectoryProperty
			IF (@_FileExists = 1) BEGIN
				SELECT @cmd = 'del "' + @_FullFilePath + '"'
				EXEC xp_cmdshell @cmd
				SET @_FullFilePath =@_FullFilePath2

				--// Recuperamos datos para mail
				SELECT						
					--@_FromAddress = [FromAddress],
					@_SmtpServer  = [SmtpServer],
					@_SmtpPort    = CAST([SmtpPort] AS NVARCHAR),
					@_LogonUser   = [LogonUser],
					@_LogonPassword	= [LogonPassword],
					@_SmtpEncryption = CASE WHEN [SmtpEncryption] = 1 THEN 'T' ELSE 'F' END
				FROM
					[dbo].[setMailOutgoing]
				WHERE
					SenderId = N'Expedicions'

				SELECT
					@_DachserMailTo = [MailTo],
					@_DachserMailFrom = [MailFrom],
					@_DachserMailCC = CASE WHEN ISNULL([MailCC], '') <> '' THEN [MailCC] ELSE [MailTo] END,
					@_MailSubject = ISNULL([MailSubject], N'')
				FROM
					[dbo].[CarrierDachserConfig] 
				WHERE
					CarrierDachserId = N'Default'
				--Enviamos el fichero generado por mail
				SELECT @cmd = @_PathBinaries + N'EmailAttachment.exe "' + @_DachserMailTo + '" "' + @_DachserMailCC + '" "carboncopy@scmlogistica.es" "' + @_MailSubject + '" "EXPEDICIONES GUIRCA" "' + @_SmtpServer + '" "' + @_SmtpPort + '" "' + @_LogonUser + '" "' + @_LogonPassword + '" "' + @_FullFilePath + '" "' + @_DachserMailFrom + '" "' + @_SmtpEncryption + '" "F"'
				PRINT N'@_PathBinaries: ' + ISNULL(@_PathBinaries, N'NULL')
				PRINT N'@MailTo: ' + ISNULL(@_DachserMailTo, N'NULL')
				PRINT N'@MailCC: ' + ISNULL(@_DachserMailCC, N'NULL')
				PRINT N'@_MailSubject: ' + ISNULL(@_MailSubject, N'NULL')
				PRINT N'@_SmtpServer: ' + ISNULL(@_SmtpServer, N'NULL')
				PRINT N'@_SmtpPort: ' + ISNULL(@_SmtpPort, N'NULL')
				PRINT N'@_LogonUser: ' + ISNULL(@_LogonUser, N'NULL')
				PRINT N'@_LogonPassword: ' + ISNULL(@_LogonPassword, N'NULL')
				PRINT N'@_FullFilePath: ' + ISNULL(@_FullFilePath, N'NULL')
				PRINT N'@MailFrom: ' + ISNULL(@_DachserMailFrom, N'NULL')
				PRINT N'@_SmtpEncryption: ' + ISNULL(@_SmtpEncryption, N'NULL')
				PRINT @cmd

				INSERT INTO SentFiles(Output)
				EXEC xp_cmdshell @cmd

				UPDATE 
					SentFiles
				SET 
					RouteSheetId = @CODHJA, 
					CarrierCode = @CODTRA, 
					SentBy = N'Email', 
					CMDCommand = @cmd, 
					ProcessId = @ProcessId 
				WHERE 
					ProcessId IS NULL
			END
		END
		


	END
FIN:

-- Retorna resultado de la ejecución.
RETURN @ERR
--FIN