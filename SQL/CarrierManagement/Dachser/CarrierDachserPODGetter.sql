﻿CREATE PROCEDURE [dbo].[CarrierDachserPODGetter]

AS

DECLARE @_PathBinaries AS NVARCHAR(255);
DECLARE @_FileExists AS INT;
DECLARE @_DirectoryProperty AS TABLE (IsFile bit, IsDir bit, HasParent bit)
DECLARE @_DownloadedFileNames AS TABLE ([fileName] nvarchar(255))
DECLARE @SQL_BULK				as nvarchar(MAX)
DECLARE @PODSourcePath			as nvarchar(255)
DECLARE @PODDestPath			as nvarchar(255)
DECLARE @PODServerFTP			as nvarchar(255)
DECLARE @PODUserFTP				as nvarchar(255)
DECLARE @PODPasswordFTP			as nvarchar(255)
DECLARE @PODPathHistorical		as nvarchar(255)
DECLARE @cmd					as nvarchar(4000)
DECLARE @Output					as nvarchar(4000)
DECLARE @FileName				as nvarchar(255)
DECLARE @ERR					as nvarchar(25)

CREATE TABLE #tmp (
	[NumeroReferenciaTransp]	nvarchar(25) null,
	[NumeroReferenciaSGA]		nvarchar(25) null,
	[Estado]					nvarchar(25) null,
	[DescripcionEstado]			nvarchar(255) null,
	[Observaciones]				nvarchar(255) null
)

SET @_PathBinaries = N'C:\SCM\TWO\'

--// cogemos datos
SELECT 
	 @PODServerFTP = PODServerFTP
	,@PODUserFTP = PODUserFTP
	,@PODPasswordFTP = PODPasswordFTP
	,@PODSourcePath = PODSourcePath
	,@PODDestPath = PODDestPath
	,@PODPathHistorical = PODPathHistorical
FROM CarrierDachserConfig

--SET @PODSourcePath = '"' + @PODSourcePath + '"'
--SET @PODDestPath = '"' + @PODDestPath + '"'
--SET @PODPathHistorical = '"' + @PODPathHistorical + '"'

PRINT 'Servidor FTP: ' + @PODServerFTP
PRINT 'Path origen: ' + @PODSourcePath
PRINT 'Path destino: ' + @PODDestPath
PRINT 'Path histórico: ' + @PODPathHistorical

--Descargamos ficheros
PRINT 'Descargando ficheros.'
EXEC [dbo].[SCM_ftp_Getfile]		
	 @FTPServer = @PODServerFTP
	,@FTPUser = @PODUserFTP
	,@FTPPWD = @PODPasswordFTP
	,@DestPath = @PODDestPath
	,@SourcePath = @PODSourcePath
	,@workdir = @PODDestPath
	,@output = @Output Output	
	,@fullCmd = @cmd Output	

PRINT 'Command: ' + @cmd

--// Extraemos los .zip descargados
SELECT @cmd = @_PathBinaries + N'TWO-Decompressor.exe "' + @PODDestPath + '" "' + @PODDestPath + '"'
EXEC xp_cmdshell @cmd

--// Obtenemos los nombres de los ficheros descargados
SET @cmd = 'dir ' + @PODDestPath + '\*.jpg /b'
INSERT INTO  @_DownloadedFileNames(fileName)
EXEC xp_cmdshell @cmd
DELETE FROM @_DownloadedFileNames WHERE fileName IS NULL
select * from @_DownloadedFileNames


--// Recorremos los ficheros descargados
PRINT 'Importando datos de los ficheros descargaods.'
DECLARE CURSOR_CRS1 CURSOR LOCAL FAST_FORWARD FOR
SELECT fileName FROM @_DownloadedFileNames
OPEN CURSOR_CRS1
FETCH NEXT FROM CURSOR_CRS1
INTO
		@FileName
WHILE (@@FETCH_STATUS = 0) 
BEGIN 
	IF (@FileName <> 'File Not Found')
	BEGIN
		SET @SQL_BULK = '
			INSERT INTO CarrierPODImages (img)
			SELECT BulkColumn FROM Openrowset(Bulk ''' + @PODDestPath + '\' + @FileName + ''', Single_Blob) as img
			INSERT INTO CarrierDachserPOD (imgId, NumeroReferenciaTransp) 
			SELECT SCOPE_IDENTITY(), ''' + SUBSTRING(@FileName, 1, LEN(@FileName) - 4) + ''''
		EXEC (@SQL_BULK)

		SET @ERR =  @@ERROR
		IF (@ERR = 0)
		BEGIN
			PRINT @FileName + ' importado.'

			--// Movemos ficheros procesados a la carpeta de histórico	
			PRINT 'Moviendo ' + @FileName + ' a historico.'
			SELECT @cmd = 'move "' + @PODDestPath + '\' + @FileName + '" "' + @PODPathHistorical + '"'
			EXEC xp_cmdshell @cmd
			PRINT 'Command: ' + @cmd

		END
		ELSE BEGIN
			PRINT @FileName + ' no importado.'
		END
	END

	FETCH NEXT FROM CURSOR_CRS1
	INTO
		@FileName
END
CLOSE CURSOR_CRS1;
DEALLOCATE CURSOR_CRS1;

--// Movemos .zip's a la carpeta de histórico	
PRINT 'Moviendo .zip''s a historico.'
SELECT @cmd = 'move ' + @PODDestPath + '\*.zip ' + @PODPathHistorical 
EXEC xp_cmdshell @cmd
PRINT 'Command: ' + @cmd

FIN:
