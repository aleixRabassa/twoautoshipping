﻿CREATE PROCEDURE [dbo].[CMI_DachserPOD]
AS
	SELECT NumeroReferenciaTransp NumeroReferenciaSGA, CreationDate, 'Referencia no encontrada.' AS ErrorMessage
	FROM CarrierDachserPOD WHERE FechaProceso IS NULL
	ORDER BY 2 DESC
RETURN 0