﻿CREATE PROCEDURE [dbo].[CarrierDachserCountryImport]
	--ENTRA
	@ImportData dbo.CountryDachser READONLY,
	@ImportProfile NVARCHAR(25)
AS
	--CONTROL
	DECLARE @TRAN AS INT			--// Indicador de transacción abierta.

	SET XACT_ABORT ON;
	SET @TRAN = @@TRANCOUNT								--// Guarda si hay transacciones abiertas.
	IF @TRAN > 0 SAVE TRAN CarrierCountryImport_TRAN	--// Guarda punto de recuperación si hay transacción abierta.
	ELSE BEGIN TRAN CarrierCountryImport_TRAN			--// Abre transacción si no la hay.

	TRUNCATE TABLE [dbo].[CarrierDachserCountry]

	INSERT INTO [dbo].[CarrierDachserCountry] ([CountryId], [CountryName], [CountryAlias], [CountryLabel], [TWOCountryId])
	SELECT 
		  dbo.[PadLeft]([CountryId], N'0', 2)
		 ,[CountryName]
		 ,[CountryAlias]
		 ,[CountryLabel]
		 ,UPPER([CountryName])
	FROM
		@ImportData

	IF @@ERROR < 0	ROLLBACK TRAN CarrierCountryImport_TRAN
	ELSE IF @TRAN = 0 COMMIT TRAN CarrierCountryImport_TRAN
RETURN 0