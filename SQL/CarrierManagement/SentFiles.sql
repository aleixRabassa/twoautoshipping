﻿CREATE TABLE [SentFiles] (
	[CarrierCode]	NVARCHAR (25)	NULL,
	[RouteSheetId]	NVARCHAR (25)   NULL,
	[SentBy]		NVARCHAR (25)	NULL,
	[CMDCommand]	NVARCHAR (4000) NULL,
	[Output]		NVARCHAR (512)  NULL,
	[CreationDate]	DATETIME        CONSTRAINT [DF_MKW_SendedMails_CreationDate] DEFAULT (getdate()) NOT NULL,
    [ProcessId]		UNIQUEIDENTIFIER NULL,
	[rowid]			BIGINT          IDENTITY (1, 1) NOT NULL,
    [rowver]		ROWVERSION      NOT NULL,
    CONSTRAINT [SentFiles_PK] PRIMARY KEY CLUSTERED ([rowid] ASC) WITH (FILLFACTOR = 90, PAD_INDEX = ON)
);