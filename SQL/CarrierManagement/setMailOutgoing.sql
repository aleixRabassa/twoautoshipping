﻿CREATE TABLE [dbo].[setMailOutgoing] (
	[rowid]							BIGINT          IDENTITY (1, 1) NOT NULL,
	[rowver]						ROWVERSION      NOT NULL,
	[CreationDate]					DATETIME        CONSTRAINT [DF_setEmailData_CreationDate] DEFAULT (getdate())		NOT	NULL,
	[LastEditDate]					DATETIME		NULL, 
	[SenderId]						NVARCHAR (25)	CONSTRAINT [DF_setMailOutgoing_SenderId] DEFAULT (N'New server')	NOT	NULL,
	[SenderDescription]				NVARCHAR (255)	CONSTRAINT [DF_setMailOutgoing_SenderDescription] DEFAULT (N'')		NOT	NULL,
	[FromName]						NVARCHAR (255)	CONSTRAINT [DF_setMailOutgoing_FromName] DEFAULT (N'')				NOT	NULL,
	[FromAddress]					NVARCHAR (255)	CONSTRAINT [DF_setMailOutgoing_FromAddress] DEFAULT (N'')			NOT	NULL,
	[SmtpServer]					NVARCHAR (255)	CONSTRAINT [DF_setMailOutgoing_SmtpServer] DEFAULT (N'')			NOT	NULL,
	[SmtpPort]						INT				CONSTRAINT [DF_setMailOutgoing_SmtpPort] DEFAULT (25)				NOT	NULL,
	[LogonUser]						NVARCHAR (255)	CONSTRAINT [DF_setMailOutgoing_LogonUser] DEFAULT (N'user')			NOT	NULL,
	[LogonPassword]					NVARCHAR (255)	CONSTRAINT [DF_setMailOutgoing_LogonPassword] DEFAULT (N'pass')		NOT	NULL,
	[SecurePasswordAuthentication]	BIT				CONSTRAINT [DF_setMailOutgoing_SPA]	DEFAULT (0)						NOT	NULL,
	[SmtpEncryption]			    BIT				CONSTRAINT [DF_setMailOutgoing_SmtpEncryption] DEFAULT (1)			NOT NULL, --None SSL/TLS (es lo mismo: SSL nomre viejo Netscape, TLS es el nombre nuevo)

	CONSTRAINT [PK_setMailOutgoing] PRIMARY KEY ([SenderId]));