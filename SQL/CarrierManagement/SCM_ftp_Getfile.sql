﻿CREATE PROCEDURE [dbo].[SCM_ftp_Getfile]

@FTPServer	varchar(128) ,
@FTPUser	varchar(128) ,
@FTPPWD		varchar(128) ,
@DestPath	varchar(128) ,
@SourcePath	varchar(128) ,

@workdir	varchar(128),

@fullCmd nvarchar(max) = '' output,
@output nvarchar(max) = '' output

as

declare	@cmd varchar(1000)
declare @workfilename varchar(128)

	select @workfilename = 'ftpcmd.txt'
	
	-- deal with special characters for echo commands
	select @FTPServer = replace(replace(replace(@FTPServer, '|', '^|'),'<','^<'),'>','^>')
	select @FTPUser = replace(replace(replace(@FTPUser, '|', '^|'),'<','^<'),'>','^>')
	--select @FTPPWD = replace(replace(replace(@FTPPWD, '|', '^|'),'<','^<'),'>','^>')
	select @DestPath = replace(replace(replace(@DestPath, '|', '^|'),'<','^<'),'>','^>')

	--open <ftpServer>
	--<FTPUser>
	--<FTPPwd>
	--cd <SourcePath>
	--lcd <DestPath>
	--prompt
	--mget *
	--quit

	select	@cmd = 'echo '					+ 'open ' + @FTPServer
			+ ' > ' + @workdir + @workfilename
	exec master.dbo.xp_cmdshell @cmd
	select	@cmd = 'echo '					+ @FTPUser
			+ '>> ' + @workdir + @workfilename
	exec master.dbo.xp_cmdshell @cmd
	select	@cmd = 'echo "'					+ @FTPPWD + '"'
			+ '>> ' + @workdir + @workfilename
	exec master.dbo.xp_cmdshell @cmd
	select	@cmd = 'echo '					+ 'cd ' + @SourcePath
			+ '>> ' + @workdir + @workfilename
	exec master.dbo.xp_cmdshell @cmd
	select	@cmd = 'echo '					+ 'lcd ' + @DestPath
			+ '>> ' + @workdir + @workfilename
	exec master.dbo.xp_cmdshell @cmd
	select	@cmd = 'echo '					+ 'prompt'
			+ '>> ' + @workdir + @workfilename
	exec master.dbo.xp_cmdshell @cmd
	select	@cmd = 'echo '					+ 'mget *' 
			+ ' >> ' + @workdir + @workfilename	
	exec master.dbo.xp_cmdshell @cmd
	select	@cmd = 'echo '					+ 'mdelete *' 
			+ ' >> ' + @workdir + @workfilename	
	exec master.dbo.xp_cmdshell @cmd
	select	@cmd = 'echo '					+ 'quit'
			+ ' >> ' + @workdir + @workfilename
	exec master.dbo.xp_cmdshell @cmd
	
	select @cmd = 'ftp -s:' + @workdir + @workfilename
	create table #a (id int identity(1,1), s varchar(1000))
	insert #a
	exec master.dbo.xp_cmdshell @cmd
	
	select @cmd = 'del ' + @workdir + @workfilename
	
	exec master.dbo.xp_cmdshell @cmd

	select id, ouputtmp = s from #a
	select @output = ISNULL(s,'') from #a where s like 'resultado%'
	set @fullCmd = 
		'open ' + @FTPServer
		+ ' ' + @FTPUser
		+ ' ' + @FTPPWD
		+ ' cd ' + @SourcePath
		+ ' lcd ' + @DestPath
		+ ' prompt'
		+ ' mget *'
		+ ' mdelete *'
		+ ' quit'