﻿CREATE TABLE [dbo].[CarrierPODImages]
(
	[ShipmentId]	UNIQUEIDENTIFIER,
	[PODimg]		IMAGE	 NULL,
	[CreationDate]	DATETIME NULL
)