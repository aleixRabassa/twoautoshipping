BEGIN TRAN ALEIX
---------------------------------------------------------------
------------------------EXPEDICI�------------------------------
---------------------------------------------------------------

--exec autoShippingShipHU '384340770000027999'
--exec autoShippingShipHU '384340770000028019'
--exec autoShippingShipHU '384340770000050478'
exec autoShippingShipHU '384340770000051864'
exec autoShippingShipHU '384340770000089515'
exec autoShippingShipHU '384340770000089522'
exec autoShippingShipHU '384340770000089539'
exec autoShippingShipHU '384340770000089546'
exec autoShippingShipHU '384340770000089553'
exec autoShippingShipHU '384340770000089560'

EXEC [scm_custom].[GUI_Carrier_SendAllFiles]

select C640CODHJA, C640ESTHJA, C640FECEXP, C640FECENT from T640HJARECREP where C640CODHJA = '160'
select C130NUMUA, C130ESTUA, C130PESUA, C130FECEXP, C130ContainerUnitId, C130CODHJA, C130ShipmentId, C130TIPUA from v130uas where C130CODHJA = '160'


ROLLBACK TRAN ALEIX

BEGIN TRAN ALEIX

---------------------------------------------------------------
-----------------------RECEPCI� DE POD-------------------------
---------------------------------------------------------------

SELECT * FROM CarrierDBSchenkerConfig
select * from DBSchenkerDescriptions

TRUNCATE TABLE CarrierDBSchenkerPOD
TRUNCATE TABLE CarrierPODImages
UPDATE Shipment SET TrackingStatus = NULL, ProofOfDelivery = 'F'

EXEC CarrierDBSchenkerPODGetter
exec DBSchenkerPOD_toShipment

SELECT * FROM CarrierDBSchenkerPOD
SELECT * FROM CarrierPODImages

select * from Shipment where ShipmentId IN ('86791','87257')


ROLLBACK TRAN ALEIX
