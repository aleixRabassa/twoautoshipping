﻿CREATE TABLE [dbo].[autoShippingCameraSettings](
	[WarehouseId] [nvarchar](25) NOT NULL,
	[CameraId] [int] NOT NULL,
	[CameraName] [nvarchar](25) NULL,
	[CameraPath] [nvarchar](255) NULL,
	[DoorId] [nvarchar](25) NULL,
	[DoorSide] [nvarchar](25) NULL,
	[Enabled] [char](1) NULL,
PRIMARY KEY CLUSTERED 
(
	[WarehouseId] ASC,
	[CameraId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[autoShippingCameraSettings] ADD  DEFAULT ('000') FOR [WarehouseId]
GO

ALTER TABLE [dbo].[autoShippingCameraSettings] ADD  CONSTRAINT [DF_autoShippingCameraSettings_Enabled]  DEFAULT ('F') FOR [Enabled]
GO