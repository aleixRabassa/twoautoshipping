﻿CREATE PROCEDURE [dbo].[autoShippingShipHU]
	-- Add the parameters for the stored procedure here
	@HUId AS NVARCHAR(25),
	@_ShipmentSent AS NVARCHAR(25) = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @CODTRA AS NVARCHAR(25)		= NULL			-- Transportistas por si se cambia.
	DECLARE @CODRUT AS NVARCHAR(25)		= NULL			-- Ruta por si se cambia.
	DECLARE @CODHJA AS NVARCHAR(25)		= NULL			--	Hoja por si se cambia.
	DECLARE @FECSER AS DATETIME			= NULL			-- Fecha de servicio por si se cambia.
	DECLARE @CODUSR AS NVARCHAR(25)		= N'AutoShipping'	-- Operario que hace la salida.
	DECLARE @FECEXP AS DATETIME			= NULL	
	DECLARE @Err AS INT = 0

    --// Si existe la UA y no está expedida
	IF EXISTS (SELECT TOP 1 1 FROM T130UAS WHERE C130FECEXP IS NULL AND C130NUMUA = @HUId)
	BEGIN
		
		--// Recogemos datos de la UA
		SELECT 
			 @CODHJA = RouteSheetId
			,@CODTRA = CarrierDelegationId  
			,@CODRUT = ISNULL(C130CODRUT,C230CODRUT)
		FROM 
			Shipment
		INNER JOIN T130UAS
			ON ShipmentId = C130ShipmentId
		INNER JOIN T230ENTSAL
			ON ShipmentId = C230ShipmentId
		WHERE 
			C130NUMUA = @HUId

		--// Expedimos la UA
		EXEC @Err = [dbo].[UA_OUT] @NUMUA = @HUId, @CODTRA = @CODTRA, @CODRUT = @CODRUT, @CODHJA = @CODHJA, @FECSER = @FECSER, @CODUSR = @CODUSR, @FECEXP = @FECEXP, @_ShipmentSent = @_ShipmentSent OUTPUT

	END

	RETURN @Err
END
