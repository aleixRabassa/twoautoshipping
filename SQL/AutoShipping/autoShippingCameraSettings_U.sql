﻿CREATE PROCEDURE [dbo].[autoShippingCameraSettings_U]
	@WarehouseId	AS NVARCHAR(25) = '000',
	@CameraId		AS int,
	@CameraName		AS NVARCHAR(255) = NULL,
	@CameraPath		AS NVARCHAR(255) = NULL,
	@DoorId			AS NVARCHAR(25) = NULL,
	@DoorSide		AS NVARCHAR(25) = NULL,
	@Enabled		AS CHAR(1)		= NULL

AS

	UPDATE autoShippingCameraSettings SET
		 CameraName		= ISNULL(@CameraName,CameraName)
		,CameraPath		= ISNULL(@CameraPath,CameraPath)
		,DoorId			= ISNULL(@DoorId,DoorId)
		,DoorSide		= ISNULL(@DoorSide,DoorSide)
		,[Enabled]		= ISNULL(@Enabled,[Enabled])
	WHERE 
			WarehouseId = @WarehouseId
		AND CameraId	= @CameraId

RETURN 0
