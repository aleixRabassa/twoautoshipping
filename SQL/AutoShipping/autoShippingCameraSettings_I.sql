﻿CREATE PROCEDURE [dbo].[autoShippingCameraSettings_I]
	@WarehouseId	AS NVARCHAR(25) = NULL,
	@CameraId		AS INT			= NULL,
	@CameraName		AS NVARCHAR(255) = NULL,
	@CameraPath		AS NVARCHAR(255) = NULL,
	@DoorId			AS NVARCHAR(25) = NULL,
	@DoorSide		AS NVARCHAR(25) = NULL,
	@Enabled		AS CHAR(1)		= NULL

AS
	DECLARE @MaxId AS INT
	SELECT @MaxId = MAX(CameraId) FROM autoShippingCameraSettings

	INSERT INTO autoShippingCameraSettings (WarehouseId,CameraId,CameraName,CameraPath,DoorId,DoorSide, [Enabled])
	VALUES (
		 ISNULL(@WarehouseId,'000')
		,ISNULL(@CameraId,@MaxId + 1)
		,ISNULL(@CameraName,'')
		,ISNULL(@CameraPath,'')
		,ISNULL(@DoorId,'1')
		,ISNULL(@DoorSide,'left')
		,ISNULL(@Enabled,'T')
	)

RETURN 0