﻿CREATE PROCEDURE [dbo].[autoShippingCameraSettings_S]
	@WarehouseId	AS NVARCHAR(25) = '000',
	@CameraId		AS NVARCHAR(25) = NULL,
	@ShowAll		AS NVARCHAR(25) = NULL
AS
	IF @ShowAll IN ('T','1','SI')
	BEGIN		
		SELECT * FROM [dbo].[autoShippingCameraSettings] 
		RETURN 0
	END

	SELECT * FROM [dbo].[autoShippingCameraSettings] 	
	LEFT OUTER JOIN (SELECT COUNT(DISTINCT DoorId) AS TotalDoors FROM autoShippingCameraSettings WHERE WarehouseId = @WarehouseId AND (CameraId = @CameraId OR @CameraId IS NULL) AND [Enabled] = 'T') T
		ON 1 = 1
	LEFT OUTER JOIN 
	(
		SELECT TOP 1 COUNT(*) AS MaxCamerasPerDoor FROM autoShippingCameraSettings
		WHERE WarehouseId = @WarehouseId AND (CameraId = @CameraId OR @CameraId IS NULL) AND [Enabled] = 'T'
		GROUP BY DoorId
		HAVING COUNT(*) >= 
			ALL(SELECT COUNT(*) FROM autoShippingCameraSettings WHERE WarehouseId = @WarehouseId AND (CameraId = @CameraId OR @CameraId IS NULL) AND [Enabled] = 'T' GROUP BY DoorId)
	) T2
		ON 1 = 1
	WHERE WarehouseId = @WarehouseId AND (CameraId = @CameraId OR @CameraId IS NULL) AND [Enabled] = 'T'

RETURN 0