﻿CREATE PROCEDURE [dbo].[autoShippingCameraSettings_D]
	@WarehouseId	AS NVARCHAR(25) = '000',
	@CameraId		AS NVARCHAR(25)
AS

	DELETE FROM autoShippingCameraSettings WHERE CameraId = @CameraId

RETURN 0