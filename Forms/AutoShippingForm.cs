﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;

namespace TwoAutoShipping
{
    public partial class AutoShippingForm : Estandar.EstandarForm
    {        
        CameraList cameraList = new CameraList();

        public AutoShippingForm()
        {
            InitializeComponent();
            lblPantalla.Text = "TWO AutoShipping";
            PlayPauseButton.Image = Image.FromFile("C:\\SCMDev\\0Wms\\TwoAutoShipping\\TwoAutoShipping\\Icons\\play.png");
            settingsButton.Image = Icons.Icons24x24.GetIcon("procesar_Gris");
            LoadButton.Image = Icons.Icons24x24.GetIcon("refrescar");
            ClearButton.Image = Image.FromFile("C:\\SCMDev\\0Wms\\TwoAutoShipping\\TwoAutoShipping\\Icons\\limpiar.png");
            
            closeButton.Image = Icons.Icons24x24.GetIcon("cerrar");

            cameraList.outterForm = this;

            LoadButton_Click(this, new EventArgs());
        }                

        public void PlayPauseButton_Click(object sender, EventArgs e)
        {
            cameraList.PlayPauseAll();

            if (cameraList.play) {
                PlayPauseButton.Text = "Pause";
                PlayPauseButton.Image = Image.FromFile("C:\\SCMDev\\0Wms\\TwoAutoShipping\\TwoAutoShipping\\Icons\\pause.png");
            }
            else
            {
                PlayPauseButton.Text = "Play";
                PlayPauseButton.Image = Image.FromFile("C:\\SCMDev\\0Wms\\TwoAutoShipping\\TwoAutoShipping\\Icons\\play.png");
            }
        }

        private void ClearButton_Click(object sender, EventArgs e)
        {
            FeedbackTbx.Text = "";
        }

        //<deprecated>
            //private void MovThresholdTbx_KeyDown(object sender, KeyEventArgs e)
            //{
            //    var HandledKeys = new List<Keys>()
            //    {
            //         Keys.D0,Keys.D1,Keys.D2,Keys.D3,Keys.D4,Keys.D5,Keys.D6,Keys.D7,Keys.D8,Keys.D9    // digits                
            //        ,Keys.Left, Keys.Right      // arrows
            //        ,Keys.Delete, Keys.Back     // remove buttons
            //        ,Keys.Enter
            //    };

            //    if (HandledKeys.Contains(e.KeyCode))
            //    {
            //        e.Handled = true;

            //        if (e.KeyCode == Keys.Enter)
            //            this.ActiveControl = null;
            //    }

            //    //cameraList.setThresholdAll(Convert.ToInt32(MovThresholdTbx.Text));
            //}

        private void LoadButton_Click(object sender, EventArgs e)
        {
            try
            {
                bool wasPlaying = cameraList.play;

                cameraList.LoadAll();

                if (cameraList.cameraCount > 0)
                {
                    ShowCameras();
                    PlayPauseButton.Enabled = true;
                }
                else
                {
                    PlayPauseButton.Enabled = false;
                }

                if (wasPlaying)
                {
                    PlayPauseButton_Click(this, new EventArgs());
                }
            }
            catch (Exception ex)
            {
                Globales.Mensajes.Msg(ex.Message);
            }
        }
        
        public void ShowCameras()
        {
            FeedbackTbx.Text += cameraList.toString();

            // vaciamos todo el contenido del grid
            cameraGrid.Controls.Clear();
            cameraGrid.ColumnStyles.Clear();
            cameraGrid.RowStyles.Clear();

            // dimensionamos el grid de cámaras
            cameraGrid.ColumnCount = cameraList.maxCamerasPerDoor;
            cameraGrid.RowCount = cameraList.totalDoors;

            PictureBox newPb;
            foreach (Camera cam in cameraList.cameras)
            {
                // creamos el picturebox de la cámara y lo mapeamos con su fotograma actual.
                newPb = new PictureBox();
                newPb.Image = cam.actualFrame.Bitmap;
                newPb.SizeMode = PictureBoxSizeMode.StretchImage;
                newPb.Dock = DockStyle.Fill;

                // damos acceso su pictureBox para que actualize los frames.
                cam.outterPictureBox = newPb;

                // gestión del grid de cámaras
                RowStyle rowStyle = new RowStyle();
                rowStyle.SizeType = SizeType.Percent;
                rowStyle.Height = (100 / cameraGrid.RowCount);
                ColumnStyle columnStyle = new ColumnStyle();
                columnStyle.SizeType = SizeType.Percent;
                columnStyle.Width = (100 / cameraGrid.ColumnCount);              

                cameraGrid.RowStyles.Add(rowStyle);
                cameraGrid.ColumnStyles.Add(columnStyle);

                cameraGrid.Dock = DockStyle.Fill;               

                // asignamos la cámara en su posicion del grid segun [side][door]
                cameraGrid.Controls.Add(newPb, cam.getDoorSideId() - 1, cam.doorIndex - 1);                
            }            
        }

        private void cameraGrid_Scroll(object sender, ScrollEventArgs e)
        {
            // TODO:
        }

        private void settingsButton_Click(object sender, EventArgs e)
        {
            TWOAutoShippingSettingsForm settingsForm = new TWOAutoShippingSettingsForm();
            settingsForm.ShowDialog();
            settingsForm.Dispose();          
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            PlayPauseButton_Click(this, new EventArgs());
            this.Close();
        }
    }
}
