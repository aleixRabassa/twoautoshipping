﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//using TwoAutoShipping.Forms;

namespace TwoAutoShipping
{
    public partial class TWOAutoShippingSettingsForm : Estandar.EstandarForm
    {
        Globales.sqlTWO sqlCameraSettings_S;
        Globales.sqlTWO sqlCameraSettings_U;
        Globales.sqlTWO sqlCameraSettings_I;
        Globales.sqlTWO sqlCameraSettings_D;

        DataTable tblCameraSettings = new DataTable();

        public TWOAutoShippingSettingsForm()
        {
            InitializeComponent();

            lblPantalla.Text = "Configuración";
            LoadDataGrid();
        }


        private void TWOAutoShippingSettingsForm_Load(object sender, EventArgs e)
        {
            NewBtn.Image        = Icons.Icons24x24.GetIcon("nuevo");
            SaveBtn.Image       = Icons.Icons24x24.GetIcon("guardar");
            DeleteBtn.Image     = Icons.Icons24x24.GetIcon("borrar");
            //EditBtn.Image       = Icons.Icons24x24.GetIcon("editar");
            RefreshBtn.Image    = Icons.Icons24x24.GetIcon("refrescar");
            CloseBtn.Image      = Icons.Icons24x24.GetIcon("cerrar");

            LoadDataGrid();
            //setReadOnly(false);
        }

        private void LoadDataGrid()
        {
            try
            {
                sqlCameraSettings_S = new Globales.sqlTWO(Globales.Definiciones.sqlConnectionG, "[dbo].[autoShippingCameraSettings_S]");
                sqlCameraSettings_S.Parameters["@ShowAll"].Value = "T";

                tblCameraSettings = sqlCameraSettings_S.Fill();
                tblCameraSettings.TableName = "A000DATA";

                bscCameraSettings.DataSource = tblCameraSettings.DefaultView;
                dgvCameraSettings.DataSource = bscCameraSettings;

                if (!dgvCameraSettings.Formato)
                {
                    dgvCameraSettings.CargaFormato();
                }

                setReadOnly(false);
            }
            catch (Exception ex)
            {
                Globales.Mensajes.Msg(ex.Message);
            }
        }

        private void setReadOnly(Boolean setTo)
        {
            for (int i = 0; i < dgvCameraSettings.Columns.Count - 1; i++)
            {
                dgvCameraSettings.Columns[i].ReadOnly = setTo;
            }
        }

        private void DeleteCamera()
        {
            if (dgvCameraSettings.Fila != -1)
            {
                sqlCameraSettings_D = new Globales.sqlTWO(Globales.Definiciones.sqlConnectionG, "[dbo].[autoShippingCameraSettings_D]");

                string cameraId = dgvCameraSettings["CameraId", dgvCameraSettings.Fila].Value.ToString();
                string cameraName = dgvCameraSettings["cameraName", dgvCameraSettings.Fila].Value.ToString();

                int ret = Globales.Mensajes.Msg("Seguro que quieres borrar la camara " + cameraName + "?", 4);
                if (ret == 6)
                {              
                    sqlCameraSettings_D.ClearParameters();
                    sqlCameraSettings_D.Parameters["@CameraId"].Value = cameraId;

                    try
                    {
                        sqlCameraSettings_D.Execute();
                        LoadDataGrid();
                    }
                    catch (Exception ex)
                    {
                        Globales.Mensajes.Msg(ex.Message);
                    }
                }
            }
        }

        private void NewCamera()
        {
            try
            {
                sqlCameraSettings_I = new Globales.sqlTWO(Globales.Definiciones.sqlConnectionG, "[dbo].[autoShippingCameraSettings_I]");
                sqlCameraSettings_I.ClearParameters();           
                
                Random random = new Random();
                sqlCameraSettings_I.Parameters["@WarehouseId"].Value    = "000";
                sqlCameraSettings_I.Parameters["@CameraId"].Value       = random.Next(0, 10000);
                sqlCameraSettings_I.Parameters["@CameraName"].Value     = "DefaultName";
                sqlCameraSettings_I.Parameters["@CameraPath"].Value     = "DefaultPath";
                sqlCameraSettings_I.Parameters["@DoorId"].Value         = "1";
                sqlCameraSettings_I.Parameters["@DoorSide"].Value       = "Left";
                sqlCameraSettings_I.Parameters["@Enabled"].Value        = "T";
                sqlCameraSettings_I.Execute();
                LoadDataGrid();
            }
            catch (Exception ex)
            {
                Globales.Mensajes.Msg(ex.Message);
            }
        }

        private void Save()
        {
            //setReadOnly(true);

            sqlCameraSettings_U = new Globales.sqlTWO(Globales.Definiciones.sqlConnectionG, "[dbo].[autoShippingCameraSettings_U]");
            sqlCameraSettings_U.ClearParameters();

            //save
            foreach (DataGridViewRow row in dgvCameraSettings.Rows)
            {
                sqlCameraSettings_U.ClearParameters();
                sqlCameraSettings_U.Parameters["@WarehouseId"].Value    = row.Cells["WarehouseId"].Value.ToString();
                sqlCameraSettings_U.Parameters["@CameraId"].Value       = Convert.ToInt32(row.Cells["CameraId"].Value);
                sqlCameraSettings_U.Parameters["@CameraName"].Value     = row.Cells["CameraName"].Value.ToString();
                sqlCameraSettings_U.Parameters["@CameraPath"].Value     = row.Cells["CameraPath"].Value.ToString();
                sqlCameraSettings_U.Parameters["@DoorId"].Value         = row.Cells["DoorId"].Value.ToString();
                sqlCameraSettings_U.Parameters["@DoorSide"].Value       = row.Cells["DoorSide"].Value.ToString();
                sqlCameraSettings_U.Parameters["@Enabled"].Value        = row.Cells["Enabled"].Value.ToString();
                sqlCameraSettings_U.Execute();
            }

            LoadDataGrid();

            //if (EditBtn.Enabled)
            //{
            //    SaveBtn.Enabled = true;
            //    EditBtn.Enabled = false;
            //}
        }

        #region Buttons

        private void NewBtn_Click(object sender, EventArgs e)
        {
            NewCamera();
        }

        private void DeleteBtn_Click(object sender, EventArgs e)
        {
            DeleteCamera();
        }

        //private void EditBtn_Click(object sender, EventArgs e)
        //{            
        //    setReadOnly(false);
        //    if (EditBtn.Enabled)
        //    {
        //        SaveBtn.Enabled = true;
        //        EditBtn.Enabled = false;
        //    }
        //}
               
        private void SaveBtn_Click(object sender, EventArgs e)
        {
            try
            {
                dgvCameraSettings.EndEdit(DataGridViewDataErrorContexts.Commit);
                Save();
            }
            catch (Exception ex)
            {
                Globales.Mensajes.Msg(ex.Message);
            }
        }

        private void RefreshBtn_Click(object sender, EventArgs e)
        {
            LoadDataGrid();
        }

        private void CloseBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion
    }
}
