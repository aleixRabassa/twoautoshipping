﻿using System.Windows.Forms;

namespace TwoAutoShipping
{
    partial class AutoShippingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AutoShippingForm));
            this.actualThresholdLbl = new System.Windows.Forms.Label();
            this.tosPantalla = new System.Windows.Forms.ToolStrip();
            this.PlayPauseButton = new System.Windows.Forms.ToolStripButton();
            this.LoadButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.ClearButton = new System.Windows.Forms.ToolStripButton();
            this.settingsButton = new System.Windows.Forms.ToolStripButton();
            this.closeButton = new System.Windows.Forms.ToolStripButton();
            this.FeedbackTbx = new System.Windows.Forms.RichTextBox();
            this.cameraGrid = new System.Windows.Forms.TableLayoutPanel();
            this.pnlPantalla.SuspendLayout();
            this.tosPantalla.SuspendLayout();
            this.SuspendLayout();
            // 
            // actualThresholdLbl
            // 
            this.actualThresholdLbl.AutoSize = true;
            this.actualThresholdLbl.Location = new System.Drawing.Point(430, 166);
            this.actualThresholdLbl.Name = "actualThresholdLbl";
            this.actualThresholdLbl.Size = new System.Drawing.Size(0, 13);
            this.actualThresholdLbl.TabIndex = 15;
            // 
            // tosPantalla
            // 
            this.tosPantalla.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.tosPantalla.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.PlayPauseButton,
            this.LoadButton,
            this.toolStripSeparator1,
            this.ClearButton,
            this.settingsButton,
            this.closeButton});
            this.tosPantalla.Location = new System.Drawing.Point(0, 86);
            this.tosPantalla.Name = "tosPantalla";
            this.tosPantalla.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.tosPantalla.Size = new System.Drawing.Size(792, 31);
            this.tosPantalla.TabIndex = 1342;
            this.tosPantalla.Text = "ToolStrip1";
            // 
            // PlayPauseButton
            // 
            this.PlayPauseButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.PlayPauseButton.Image = ((System.Drawing.Image)(resources.GetObject("PlayPauseButton.Image")));
            this.PlayPauseButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.PlayPauseButton.Name = "PlayPauseButton";
            this.PlayPauseButton.Size = new System.Drawing.Size(28, 28);
            this.PlayPauseButton.Text = "Play";
            this.PlayPauseButton.Click += new System.EventHandler(this.PlayPauseButton_Click);
            // 
            // LoadButton
            // 
            this.LoadButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.LoadButton.Image = ((System.Drawing.Image)(resources.GetObject("LoadButton.Image")));
            this.LoadButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.LoadButton.Name = "LoadButton";
            this.LoadButton.Size = new System.Drawing.Size(28, 28);
            this.LoadButton.Text = "Actualizar";
            this.LoadButton.Click += new System.EventHandler(this.LoadButton_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 31);
            // 
            // ClearButton
            // 
            this.ClearButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ClearButton.Image = ((System.Drawing.Image)(resources.GetObject("ClearButton.Image")));
            this.ClearButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ClearButton.Name = "ClearButton";
            this.ClearButton.Size = new System.Drawing.Size(28, 28);
            this.ClearButton.Text = "Vaciar feedback";
            this.ClearButton.Click += new System.EventHandler(this.ClearButton_Click);
            // 
            // settingsButton
            // 
            this.settingsButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.settingsButton.Image = ((System.Drawing.Image)(resources.GetObject("settingsButton.Image")));
            this.settingsButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.settingsButton.Name = "settingsButton";
            this.settingsButton.Size = new System.Drawing.Size(28, 28);
            this.settingsButton.Text = "Configuración";
            this.settingsButton.Click += new System.EventHandler(this.settingsButton_Click);
            // 
            // closeButton
            // 
            this.closeButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.closeButton.Image = ((System.Drawing.Image)(resources.GetObject("closeButton.Image")));
            this.closeButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(28, 28);
            this.closeButton.Text = "Cerrar";
            this.closeButton.Click += new System.EventHandler(this.closeButton_Click);
            // 
            // FeedbackTbx
            // 
            this.FeedbackTbx.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.FeedbackTbx.Dock = System.Windows.Forms.DockStyle.Right;
            this.FeedbackTbx.Location = new System.Drawing.Point(494, 117);
            this.FeedbackTbx.Name = "FeedbackTbx";
            this.FeedbackTbx.Size = new System.Drawing.Size(298, 386);
            this.FeedbackTbx.TabIndex = 1343;
            this.FeedbackTbx.Text = "";
            // 
            // cameraGrid
            // 
            this.cameraGrid.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.cameraGrid.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.cameraGrid.Location = new System.Drawing.Point(0, 120);
            this.cameraGrid.Name = "cameraGrid";
            this.cameraGrid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.cameraGrid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.cameraGrid.Size = new System.Drawing.Size(200, 100);
            this.cameraGrid.TabIndex = 1344;
            // 
            // AutoShippingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(792, 532);
            this.Controls.Add(this.cameraGrid);
            this.Controls.Add(this.FeedbackTbx);
            this.Controls.Add(this.tosPantalla);
            this.Controls.Add(this.actualThresholdLbl);
            this.Name = "AutoShippingForm";
            this.Text = "AutoShippingForm";
            this.Controls.SetChildIndex(this.actualThresholdLbl, 0);
            this.Controls.SetChildIndex(this.pnlPantalla, 0);
            this.Controls.SetChildIndex(this.tosPantalla, 0);
            this.Controls.SetChildIndex(this.FeedbackTbx, 0);
            this.Controls.SetChildIndex(this.cameraGrid, 0);
            this.pnlPantalla.ResumeLayout(false);
            this.pnlPantalla.PerformLayout();
            this.tosPantalla.ResumeLayout(false);
            this.tosPantalla.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label actualThresholdLbl;
        internal ToolStrip tosPantalla;
        private ToolStripButton PlayPauseButton;
        private ToolStripButton LoadButton;
        public RichTextBox FeedbackTbx;
        private TableLayoutPanel cameraGrid;
        private ToolStripSeparator toolStripSeparator1;
        private ToolStripButton ClearButton;
        private ToolStripButton settingsButton;
        private ToolStripButton closeButton;
    }
}

