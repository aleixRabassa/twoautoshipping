﻿namespace TwoAutoShipping
{
    partial class TWOAutoShippingSettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TWOAutoShippingSettingsForm));
            this.bscCameraSettings = new System.Windows.Forms.BindingSource(this.components);
            this.dgvCameraSettings = new ControlesTWO.DataGridViewTwo();
            this.tosPantalla = new System.Windows.Forms.ToolStrip();
            this.NewBtn = new System.Windows.Forms.ToolStripButton();
            this.DeleteBtn = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.SaveBtn = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.RefreshBtn = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.CloseBtn = new System.Windows.Forms.ToolStripButton();
            this.pnlPantalla.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bscCameraSettings)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCameraSettings)).BeginInit();
            this.tosPantalla.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlPantalla
            // 
            this.pnlPantalla.Size = new System.Drawing.Size(800, 31);
            // 
            // dgvCameraSettings
            // 
            this.dgvCameraSettings.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvCameraSettings.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvCameraSettings.Checkable = 0;
            this.dgvCameraSettings.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCameraSettings.Fila = -1;
            this.dgvCameraSettings.Formato = false;
            this.dgvCameraSettings.Location = new System.Drawing.Point(12, 120);
            this.dgvCameraSettings.MultiColumnSort = null;
            this.dgvCameraSettings.MultiRow = false;
            this.dgvCameraSettings.MultiSelect = false;
            this.dgvCameraSettings.Name = "dgvCameraSettings";
            this.dgvCameraSettings.OrderColumns = true;
            this.dgvCameraSettings.Size = new System.Drawing.Size(776, 298);
            this.dgvCameraSettings.Sortable = true;
            this.dgvCameraSettings.TabIndex = 1336;
            this.dgvCameraSettings.TamanoLetra = 9.75F;
            this.dgvCameraSettings.Titulo = null;
            // 
            // tosPantalla
            // 
            this.tosPantalla.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.tosPantalla.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.NewBtn,
            this.DeleteBtn,
            this.ToolStripSeparator3,
            this.SaveBtn,
            this.toolStripSeparator1,
            this.RefreshBtn,
            this.toolStripSeparator4,
            this.CloseBtn});
            this.tosPantalla.Location = new System.Drawing.Point(0, 86);
            this.tosPantalla.Name = "tosPantalla";
            this.tosPantalla.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.tosPantalla.Size = new System.Drawing.Size(800, 31);
            this.tosPantalla.TabIndex = 1337;
            this.tosPantalla.Text = "ToolStrip1";
            // 
            // NewBtn
            // 
            this.NewBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.NewBtn.Image = ((System.Drawing.Image)(resources.GetObject("NewBtn.Image")));
            this.NewBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.NewBtn.Name = "NewBtn";
            this.NewBtn.Size = new System.Drawing.Size(28, 28);
            this.NewBtn.Text = "Nuevo";
            this.NewBtn.Click += new System.EventHandler(this.NewBtn_Click);
            // 
            // DeleteBtn
            // 
            this.DeleteBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.DeleteBtn.Image = ((System.Drawing.Image)(resources.GetObject("DeleteBtn.Image")));
            this.DeleteBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.DeleteBtn.Name = "DeleteBtn";
            this.DeleteBtn.Size = new System.Drawing.Size(28, 28);
            this.DeleteBtn.Text = "Borrar";
            this.DeleteBtn.Click += new System.EventHandler(this.DeleteBtn_Click);
            // 
            // ToolStripSeparator3
            // 
            this.ToolStripSeparator3.Name = "ToolStripSeparator3";
            this.ToolStripSeparator3.Size = new System.Drawing.Size(6, 31);
            // 
            // SaveBtn
            // 
            this.SaveBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.SaveBtn.Image = ((System.Drawing.Image)(resources.GetObject("SaveBtn.Image")));
            this.SaveBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.SaveBtn.Name = "SaveBtn";
            this.SaveBtn.Size = new System.Drawing.Size(28, 28);
            this.SaveBtn.Text = "Guardar";
            this.SaveBtn.Click += new System.EventHandler(this.SaveBtn_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 31);
            // 
            // RefreshBtn
            // 
            this.RefreshBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.RefreshBtn.Image = ((System.Drawing.Image)(resources.GetObject("RefreshBtn.Image")));
            this.RefreshBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.RefreshBtn.Name = "RefreshBtn";
            this.RefreshBtn.Size = new System.Drawing.Size(28, 28);
            this.RefreshBtn.Text = "Refrescar";
            this.RefreshBtn.Click += new System.EventHandler(this.RefreshBtn_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 31);
            // 
            // CloseBtn
            // 
            this.CloseBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.CloseBtn.Image = ((System.Drawing.Image)(resources.GetObject("CloseBtn.Image")));
            this.CloseBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.CloseBtn.Name = "CloseBtn";
            this.CloseBtn.Size = new System.Drawing.Size(28, 28);
            this.CloseBtn.Click += new System.EventHandler(this.CloseBtn_Click);
            // 
            // TWOAutoShippingSettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.tosPantalla);
            this.Controls.Add(this.dgvCameraSettings);
            this.Name = "TWOAutoShippingSettingsForm";
            this.Text = "TWOAutoShippingSettingsForm";
            this.Load += new System.EventHandler(this.TWOAutoShippingSettingsForm_Load);
            this.Controls.SetChildIndex(this.dgvCameraSettings, 0);
            this.Controls.SetChildIndex(this.pnlPantalla, 0);
            this.Controls.SetChildIndex(this.tosPantalla, 0);
            this.pnlPantalla.ResumeLayout(false);
            this.pnlPantalla.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bscCameraSettings)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCameraSettings)).EndInit();
            this.tosPantalla.ResumeLayout(false);
            this.tosPantalla.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource bscCameraSettings;
        internal ControlesTWO.DataGridViewTwo dgvCameraSettings;
        internal System.Windows.Forms.ToolStrip tosPantalla;
        private System.Windows.Forms.ToolStripButton NewBtn;
        private System.Windows.Forms.ToolStripButton DeleteBtn;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator3;
        private System.Windows.Forms.ToolStripButton SaveBtn;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton RefreshBtn;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripButton CloseBtn;
    }
}